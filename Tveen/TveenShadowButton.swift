//
//  TveenShadowButton.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 21/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

@IBDesignable class TveenShadowButton: UIView{
    var view: UIView!
    
    @IBOutlet weak var butt: UIButton!
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        view.backgroundColor = UIColor.clear
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenShadowButton", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    @IBInspectable var buttColor: UIColor = UIColor.white{
        didSet{
            butt.backgroundColor  = buttColor
        }
    }
    
    @IBInspectable var shadowOffsetX: CGFloat = CGFloat(){
        didSet{
            view.layer.masksToBounds = false
            view.layer.shadowOffset = CGSize(width: shadowOffsetX, height: view.layer.shadowOffset.height)
        }
    }
    @IBInspectable var shadowOffsetY: CGFloat = CGFloat(){
        didSet{
            view.layer.masksToBounds = false
            view.layer.shadowOffset = CGSize(width: view.layer.shadowOffset.width, height: shadowOffsetY)
        }
    }
    @IBInspectable var buttText: String = "Text"{
        didSet{
            butt.setTitle(buttText, for: UIControlState())
        }
    }
    
    //Shadow code
    
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            view.layer.masksToBounds = false
            view.layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0 {
        didSet {
            view.layer.masksToBounds = false
            view.layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            butt.layer.cornerRadius = cornerRadius
            butt.layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var textAlignment: String = "Center"{
        didSet{
            switch textAlignment{
            case "Left": butt.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            case "Right": butt.contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            default: butt.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
            }
        }
    }
    
    @IBInspectable var imageColor: UIColor? {
        didSet {
            let origImage = butt.image(for: UIControlState())
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            butt.setImage(tintedImage, for: UIControlState())
            tintColor = imageColor
        }
    }
    
    @IBInspectable var image: UIImage? {
        didSet {
            butt.setImage(image, for: UIControlState())
        }
    }
    
    @IBInspectable var opacity: CGFloat = 255 {
        didSet {
            butt.backgroundColor = backgroundColor?.withAlphaComponent(opacity/255)
        }
    }
    
    @IBInspectable var maskPixels: CGFloat = 0 {
        didSet {
            butt.maskBottomPixels(maskPixels)
        }
    }
    
    @IBInspectable var highlightedColor: UIColor = UIColor.white {
        didSet {
            butt.setBackgroundColor(highlightedColor.withAlphaComponent(1), forState: .highlighted)
        }
    }
    
    @IBInspectable var disabledColor: UIColor? {
        didSet{
            butt.setBackgroundColor(disabledColor!, forState: .disabled)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            butt.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            butt.layer.borderColor = borderColor?.cgColor
        }
    }
}
