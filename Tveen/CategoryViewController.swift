//
//  CategoryViewController.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 11/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

protocol CategoryViewControllerRouterDelegate {
        func showMainControllerFromCategoryWith(model:LoginModel)
}

class CategoryViewController: UITableViewController {
    
    let categoryNames = ["Развлечения и отдых", "Ночная жизнь", "Спорт", "Культура", "Образование и бизнес", "Для детей", "Другое"]
    let categoryImages = ["entertainment.png", "nightlife.png", "sport.png", "art.png", "education.png", "children.png", "other.png"]
    var selectedCategory = [Int]()
    
    var routerDelegate:CategoryViewControllerRouterDelegate!
    
    var registrationModel: LoginModel?
    
    var selectedRow = [Bool]()
    
    var isOpenFromRegistation = true
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        createSelectedArray()
    }
    
    override func viewWillAppear(_ animated: Bool){
        setupNavigationController()
    }
    
    //MARK: - Private
    
    private func createSelectedArray() {
        for _ in 0 ... categoryNames.count {
            selectedRow.append(true)
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryNames.count
    }
    
    func setupNavigationController(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.isHidden = false
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(donePressed))
        
//        if isOpenFromRegistation {
//            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Выйти", style: .done, target: self, action: #selector(dismissThis))
//        } else {
            navigationItem.hidesBackButton = true
//            navigationItem.leftBarButtonItem = UIBarButtonItem()
//        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 480, height: 44))
        label.numberOfLines = 2
        label.text =  "Что вам интересно?"
        label.textAlignment = .center
        label.font = UIFont(name: "SFUIText-Regular", size: 15)
        label.textColor = UIColor.white
        
        navigationItem.titleView = label
    }
    
    func dismissThis(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func donePressed(){
        if selectedCategory.count < 1 {
            self.showAlert(title: "Ошибка", message: "Выберите хотя бы одну категорию")
        } else {
            UserManager.sharedManager.editUser(UUID: (registrationModel?.uuid)!, type: .likes, parameters: ["category":selectedCategory], success: { (data) in
                print(data)
                self.registrationModel?.category = data.category
                self.routerDelegate.showMainControllerFromCategoryWith(model: self.registrationModel!)
            }, failure: { (error) in
                self.showAlert(title: "Ошибка", message: error.domain)
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return view.frame.width / 750 * 230
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoryCell
        
        cell.im.image = UIImage(named: categoryImages[indexPath.row])
        
        cell.im.contentMode = .scaleAspectFit
        cell.name.text = categoryNames[indexPath.row]
        cell.contentView.layoutIfNeeded()
        
        cell.selectedView.isHidden = selectedRow[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedCategory.append(indexPath.row + 1)
        selectedRow[indexPath.row] = false
        
        let cell = tableView.cellForRow(at: indexPath) as! CategoryCell
        cell.selectedView.isHidden = false
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        var i = 0
        selectedRow[indexPath.row] = true
        for category in selectedCategory {
            if category == indexPath.row + 1 {
                selectedCategory.remove(at: i)
            }
            
            i = i + 1
        }
        let cell = tableView.cellForRow(at: indexPath) as! CategoryCell
        cell.selectedView.isHidden = true
    }
}
