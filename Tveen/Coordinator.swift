//
//  Coordinator.swift
//  Tveen
//
//  Created by Lyonich on 15.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class Coordinator: NSObject, LoginRouterDelegate, RegistrationRouterDelegate, CategoryViewControllerRouterDelegate, PhoneConfirmationControllerRouterDelegate {
    
    var window: UIWindow!
    
    static let sharedInstance = Coordinator()
    
    lazy var loginViewController: LoginViewController = {
        let loginController = Controllers.login
        loginController.routerDelegate = self
        
        return loginController
    }()
    
    lazy var confirmationPhoneController: PhoneConfirmationViewController = {
        let confirmationController = Controllers.confirmationPhone
        confirmationController.routerDelegate = self
        
        return confirmationController
    }()
    
    lazy var registrationViewController: RegistrationViewController = {
       
        let registrationController = Controllers.registration
        
        registrationController.routerDelegate = self
        
        return registrationController
    }()
    
    
    lazy var categoryViewController: CategoryViewController = {
        let controller = Controllers.changeCategory
        controller.routerDelegate = self
        return controller
    }()
    
    lazy var searchViewController: UINavigationController = {
        let controller = Controllers.searchViewController
        return controller
    }()
    
    
    
    func startCoordinatorWithWindow(_ window: UIWindow) {
        self.window = window
        self.window.rootViewController = loginViewController
    }
    
    //MARK: - PhoneConfirmationControllerRouterDelegate
    
    func backToParentController() {
        confirmationPhoneController.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - LoginRouterDelegate
    
    func showMainControllerWith(model: LoginModel) {
        
        self.window.rootViewController = searchViewController
    }
    
    func showRegistrationController() {
        registrationViewController.typeRegistration = .Login
        let registrationNavigationController = UINavigationController(rootViewController: registrationViewController)
        loginViewController.present(registrationNavigationController, animated: true, completion: nil)
    }
    
    //MARK: - RegistrationRouterDelegate
    
    func showConfirmationPhone(registrationModel:LoginModel) {
        registrationViewController.navigationController?.pushViewController(confirmationPhoneController, animated: true)
    }
    
    func closeRegistration() {
        registrationViewController.dismiss(animated: true, completion: nil)
    }
    
    func showMainControllerFromRegistrationWith(model:LoginModel) {
        self.window.rootViewController = searchViewController
    }
    
    func showChangeCategory(registrationModel:LoginModel) {
        categoryViewController.registrationModel = registrationModel
        categoryViewController.isOpenFromRegistation = true
        registrationViewController.navigationController?.pushViewController(categoryViewController, animated: true)
    }
    
    //MARK: - CategoryViewControllerRouterDelegate
    
    func showMainControllerFromCategoryWith(model:LoginModel) {
        self.window.rootViewController = searchViewController
    }


}
