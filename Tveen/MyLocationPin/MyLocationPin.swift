//
//  MyLocationPin.swift
//  Tveen
//
//  Created by Lyonich on 25.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class MyLocationPin: UIView {

  @IBOutlet weak var avatarImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    avatarImageView.layer.cornerRadius = 9
    avatarImageView.layer.masksToBounds = true
  }

}
