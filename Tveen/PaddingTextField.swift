//
//  PaddingTextField.swift
//  FighterParser
//
//  Created by Leonid Kibukevich on 08.01.17.
//  Copyright © 2017 Lyonich. All rights reserved.
//

import UIKit

//@IBDesignable

class PaddingTextField: UITextField {

  var padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
  
  override func textRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  override func editingRect(forBounds bounds: CGRect) -> CGRect {
    return UIEdgeInsetsInsetRect(bounds, padding)
  }
  
  @IBInspectable var leftInset: CGFloat {
    get {
      return padding.left
    }
    set {
      padding.left = newValue
    }
  }
  
  @IBInspectable var placeHolderColor: UIColor? {
    get {
      return self.placeHolderColor
    }
    set {
      self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSForegroundColorAttributeName: newValue!])
    }
  }

}
