//
//  TveenScrollView.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 11/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

class TveenScrollView: UIView {
    
    var view: UIView!
    
    @IBOutlet weak var containerView: UIView!
    override func addSubview(_ view: UIView) {
        
        containerView.addSubview(view)
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenScrollView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
