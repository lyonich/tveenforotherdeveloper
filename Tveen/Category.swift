//
//  Category.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 25/01/2017.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class Category
{
    var id = Int()
    var name = String()
    var image = UIImage()
    
    //для картинки любого цвета (настраивается с помощью imageView.tintColor)
    var templateImage: UIImage {
        get{
            return image.withRenderingMode(.alwaysTemplate)
        }
    }
    
    init(){}
    
    init(id: Int) {
        self.id = id
        switch id {
        case 1:
            name = "Развлечения и отдых"
            image = #imageLiteral(resourceName: "c1")
        case 2:
            name = "Ночная жизнь"
            image = #imageLiteral(resourceName: "c2")
        case 3:
            name = "Спорт"
            image = #imageLiteral(resourceName: "c4")
        case 4:
            name = "Искусство"
            image = #imageLiteral(resourceName: "c5")
        case 5:
            name = "Образование и бизнес"
            image = #imageLiteral(resourceName: "c7")
        case 6:
            name = "Для детей"
            image = #imageLiteral(resourceName: "c3")
        case 7: name = "Другое"
            image = #imageLiteral(resourceName: "c2")
        default: break
        }
    }
}
