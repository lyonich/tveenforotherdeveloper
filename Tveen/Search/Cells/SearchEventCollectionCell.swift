//
//  SearchEventCollectionCell.swift
//  Tveen
//
//  Created by Lyonich on 26.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class SearchEventCollectionCell: UICollectionViewCell {

  @IBOutlet weak var typeIndicatorImageView: UIImageView!
  @IBOutlet weak var avatarOfEventImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var countOfVisitorsLabel: UILabel!
  @IBOutlet weak var backView: UIView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    addShadowForbackground()
  }
  
  //MARK: - Private
  
  private func addShadowForbackground() {
    backView.layer.cornerRadius = 8
    backView.layer.shadowColor = UIColor.black.cgColor
    backView.layer.shadowRadius = 10
    backView.layer.shadowOpacity = 0.8
    backView.layer.shadowOffset = CGSize(width: 0, height: 1)
  }

}
