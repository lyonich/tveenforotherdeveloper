//
//  SearchSortedView.swift
//  Tveen
//
//  Created by Lyonich on 27.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

protocol SearchSortedViewDeleagte {
    func didRemoveView()
}

class SearchSortedView: UIView {

    @IBOutlet var selectedMarkCollection: [UIImageView]!
    @IBOutlet var sortedButtons: [UIButton]!
    @IBOutlet weak var sortedContainerView: UIView!
    
    var delegate:SearchSortedViewDeleagte!
      
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    //MARK: - Public
    
    func removeSelf() {
        UIView.animate(withDuration: 0.5, animations: {
            self.sortedContainerView.frame.origin.y = -328
        }, completion: { (finised) in
            self.removeFromSuperview()
            self.delegate.didRemoveView()
        })
    }

    //MARK: - Actions
      
    @IBAction func checkButtonAction(_ sender: UIButton) {
        for mark in selectedMarkCollection {
            if mark.tag == sender.tag {
                mark.isHidden = !mark.isHidden
            } else {
                mark.isHidden = true
            }
        }
        
        for button in sortedButtons {
            if button.tag + 2 == sender.tag {
                button.isHidden = !button.isHidden
            } else {
                button.isHidden = true
            }
        }
    }
      
    @IBAction func changeOrderSortedButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
  
    @IBAction func backgroundButtonAction(_ sender: Any) {
        self.removeSelf()
    }
}
