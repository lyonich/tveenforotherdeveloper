//
//  ScopeSearchBar.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 12/03/2017.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class ScopeSearchBar: UISearchBar {

    
    
    override func layoutSubviews() {
        self.subviews[0].subviews[0].isHidden = false
        self.showsScopeBar = true
    }
    

    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
