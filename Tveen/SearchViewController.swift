//
//  SearchViewController.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 25/11/2016.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import NWSTokenView
import GoogleMaps
import Kingfisher


protocol SearchViewControllerRouterDelegate {
    func showDetailEvent(event: Event)
    
    //TODO: Вызывать в делегатном методе от ячейки для показа юзера либо в этом методе передавать уже модель юзера, а загружать ее на этом экране
    func showUserProfile(model: LoginModel)
}


class SearchViewController: UIViewController {
    
    @IBOutlet weak var entertainmentButton: UIButton!
    
    @IBOutlet weak var nightLifeButton: UIButton!
    @IBOutlet weak var sportButton: UIButton!
    @IBOutlet weak var artButton: UIButton!
    @IBOutlet weak var forKidsButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var educationAndBusinessButton: UIButton!

    @IBOutlet weak var tokenView: NWSTokenView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tokenContainer: UIView!
    
    @IBOutlet weak var tipsTableView: UITableView!
    @IBOutlet weak var tokenViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuTopConstraint: NSLayoutConstraint!
    @IBOutlet var panGesture: UIPanGestureRecognizer!
    
    @IBOutlet var mapSearchView: SearchMapView!
    
    @IBOutlet weak var tipsTableBottomConstraint: NSLayoutConstraint!

    @IBOutlet var searchBarButton: UIButton!
    
    @IBOutlet weak var topSegmentedControl: UISegmentedControl!
    @IBOutlet weak var topImageView: UIImageView!
    
    
    var routerDelegate:SearchViewControllerRouterDelegate!
    
    var sortedView = SearchSortedView.loadFromNibNamed(nibNamed: "SearchSortedView")! as! SearchSortedView
    
    let tokenViewMinHeight: CGFloat = 30
    let tokenViewMaxHeight: CGFloat = 150.0
    
    var handleHeight: CGFloat = 31
    var myTags = [String]()
    var drawerIsOpen = true
    var addTagButton = UIButton()
    
    var isAddPinMode: Bool = false
    
    var selectedCategories = [Bool]()
    
    let tipPlaces = ["ул. Вешняковского", "пр. Вернадского"]
    let tipTags = ["Вечеринка", "Велосипед", "Вермишель"]
    
    var colorForTitle = [String: UIColor]()
    
    var events = [Event]()
    
    var shouldMoveDrawer = false
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        sortedView.delegate = self
        
        let categoryButtons = [entertainmentButton, nightLifeButton, sportButton, artButton, educationAndBusinessButton, forKidsButton, otherButton]
        
        for button in categoryButtons{
            button!.isSelected = selectedCategories[button!.tag]
        }
        
        tokenView.dataSource = self
        tokenView.delegate = self
        tokenView.textView.returnKeyType = .done
        tokenView.reloadData()
        
        setupNavigationController()
        createAddTagButton()
        
        addMapView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tokenView.reloadData()
        tokenView.layoutSubviews()
        view.layoutIfNeeded()
        
        tableView.reloadData()
        tokenView.textView.isEditable = false
        
        configurateTableView()
        
        var categories = [Int]()
        
        for i in 0..<selectedCategories.count{
            if selectedCategories[i]{
                categories.append(i+1)
            }
        }
        
        EventManager.sharedManager.getEvents(parameters: ["type": 17, "sort_type":3,"order_type":1, "categories": categories, "tags" : myTags], success: { events in
            self.events = events
            self.tableView.reloadData()
        }) {  error in
            print(error)
        }
    }
    
    //MARK: - Private
    
    @IBAction func categoryPressed(_ sender: Any) {
        selectedCategories[(sender as! UIButton).tag] =  !selectedCategories[(sender as! UIButton).tag]
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
    }
    
    private func addMapView() {
        mapSearchView.frame = CGRect(x: 0, y: 30, width: Size.Display.width, height: Size.Display.height - 30)
        self.view.insertSubview(mapSearchView, at: 1)
        mapSearchView.isHidden = true
        mapSearchView.delegate = self
        mapSearchView.delegateView = self
        
       
    }
    

    private func configurateTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 364
    }
    
    private func createAddTagButton() {
        addTagButton = UIButton(type: UIButtonType.custom)
        addTagButton.setImage(#imageLiteral(resourceName: "addTag"), for: .normal)
        tokenView.addSubview(addTagButton)
        setNewFrameAddTagButton()
        addTagButton.addTarget(self, action: #selector(addTagButtonAction(_:)), for: .touchUpInside)
    }
    
    fileprivate func setNewFrameAddTagButton() {
        addTagButton.frame = CGRect(x: tokenView.textView.frame.origin.x - 2, y: tokenView.textView.frame.origin.y - 2, width: 29, height: 29)
    }
    
    fileprivate func endEditTags() {
        tokenView.textView.endEditing(true)
        tokenView.textView.isEditable = false
        addTagButton.isHidden = false
    }
    
    //MARK: - Actions
    
    @IBAction func sortedButtonAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        
        if !self.sortedView.isDescendant(of: self.view) {
            sortedView.frame = CGRect(x: 0, y: 0, width: Size.Display.width, height: Size.Display.height)
            view.addSubview(sortedView)
            
            UIView.animate(withDuration: 0.5, animations: { 
                self.sortedView.frame = CGRect(x: 0, y: 0, width: Size.Display.width, height: Size.Display.height)
            })
        } else {
            self.sortedView.removeSelf()
        }
    }
    
    @IBAction func valueChangedSegmentControlAction(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 1 {
            mapSearchView.isHidden = false
        } else {
            mapSearchView.isHidden = true
            mapSearchView.eventOneLineCollectionView.isHidden = true
            mapSearchView.setStateForCircleButtons(isTopState: false)
        }
    }
    
    @IBAction func addTagButtonAction(_ sender: UIButton) {
        tokenView.textView.isEditable = true
        tokenView.textView.becomeFirstResponder()
        
        
        
       // tokenView(tokenView)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(searchDone))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(searchCancelled))
        
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        addTagButton.isHidden = true
    }
    
    @IBAction func panMenu(_ sender: Any){
        isAddPinMode = false
        mapSearchView.flagButton.isSelected = false
        mapSearchView.flagButton.layer.shadowColor = UIColor.black.cgColor
        mapSearchView.mapMessageView.isHidden =  true
        
        if let recognizer = sender as? UIPanGestureRecognizer{
            if recognizer.state == .ended{
                drawerIsOpen = !recognizer.wentUp(theViewYouArePassing: tokenContainer)
                menuTopConstraint.constant = drawerIsOpen ? 0 : handleHeight - tokenContainer.frame.height
                UIView.animate(withDuration: 0.2, animations: {
                    self.view.layoutIfNeeded()
                })
            } else {
                let pan = recognizer.translation(in: view)
                menuTopConstraint.constant =  pan.y - (drawerIsOpen ? CGFloat(0) : tokenContainer.frame.height - 30 )
                print("pan.y is \(pan.y)")
                print("pan with \(menuTopConstraint.constant)")
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func searchDone(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "extendedSearch"), landscapeImagePhone: #imageLiteral(resourceName: "extendedSearch"), style: .plain, target: self, action: #selector(openExtendedSearch))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(searchCancelled))
        self.tokenViewDidEndEditing(tokenView)
        tokenView.resignFirstResponder()
    }
    
    func searchCancelled(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "extendedSearch"), landscapeImagePhone: #imageLiteral(resourceName: "extendedSearch"), style: .plain, target: self, action: #selector(openExtendedSearch))
        navigationItem.leftBarButtonItem = nil
        self.tokenViewDidEndEditing(tokenView)
        tokenView.resignFirstResponder()
    }
    
    func openDrawer() {
        menuTopConstraint.constant = 0
        
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func setupNavigationController() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "extendedSearch"), landscapeImagePhone: #imageLiteral(resourceName: "extendedSearch"), style: .plain, target: self, action: #selector(openExtendedSearch))
        
        navigationItem.titleView = searchBarButton
    }
    
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            tipsTableBottomConstraint.constant = keyboardSize.height
            UIView.animate(withDuration: 0.2, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            tipsTableBottomConstraint.constant = 0
            UIView.animate(withDuration: 0.2, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func openExtendedSearch() {
        performSegue(withIdentifier: "ExtendedSearch", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let button = UIBarButtonItem()
        button.title = ""
        button.style = .plain
        navigationItem.backBarButtonItem = button
        
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tipsTableView{
            return 2
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == tipsTableView{
            return section == 0 ? "Места" : "Теги"
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tipsTableView{
            return section == 0 ? 2 : 3
        }
        else{
            return events.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tipsTableView{
            if indexPath.section == 0{
                self.tokenView.textView.text = tipPlaces[indexPath.row] + " "
                self.tokenView(tokenView, didChangeText: tipPlaces[indexPath.row] + " ")
                tableView.deselectRow(at: indexPath, animated: true)
                
                tokenView.tokenForIndex(myTags.count - 1).backgroundColor = UIColor(red: 156/255, green: 101/255, blue: 245/255, alpha: 1)
                colorForTitle[tipPlaces[indexPath.row]] = UIColor(red: 156/255, green: 101/255, blue: 245/255, alpha: 1)
                
            }
            else{
                self.tokenView.textView.text = tipTags[indexPath.row] + " "
                self.tokenView(tokenView, didChangeText: tipTags[indexPath.row] + " ")
                tableView.deselectRow(at: indexPath, animated: true)
                
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tipsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tipsCell", for: indexPath) as! TipsCell
            cell.tipLabel.text = indexPath.section == 0 ? tipPlaces[indexPath.row] : tipTags[indexPath.row]
            cell.tipImageView.image =  indexPath.section == 0  ? #imageLiteral(resourceName: "pinForTips") : #imageLiteral(resourceName: "#")
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCell", for: indexPath) as! FeedLCell
            let event = events[indexPath.row]
            cell.name.text = event.title
            cell.aboutString = event.description
            cell.myTags = event.tags
            
            cell.dateLabel.text = convertDateFormater(date: event.timeStart, finalDateFormate: Dates.feedDateFormat)
            cell.tags.editable = false
            cell.tags.delegate = cell
            cell.tags.dataSource = cell
            cell.tags.layoutIfNeeded()
            cell.tags.reloadData()
            cell.categoryIm.image =  event.category.image
            //cell.creatorLogin.text = event.ownerLogin
            //cell.creatorName.text = event.ownerName
            //cell.creatorIm.kf.setImage(with: URL(string:event.ownerAvatarLink), placeholder: #imageLiteral(resourceName: "placeholder"))
            cell.im.kf.setImage(with: URL(string:event.mainImageURL), placeholder: #imageLiteral(resourceName: "mainPhotoPlaceholder"))
            cell.willGo.text = String(event.members.count)
            cell.owner_UUID = event.owner_uuid
//            cell.showProgileDelegate = self
            
            return cell
        }
    }
}

extension SearchViewController: NWSTokenDataSource, NWSTokenDelegate {
    
    // MARK: NWSTokenDataSource
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int {
        return myTags.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets? {
        return UIEdgeInsetsMake(4, 3, 0, 0)
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String? {
        return ""
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String? {
        return ""
    }
    
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView? {
        if let token = NWSImageToken.initWithTitle(myTags[Int(index)], image: nil) {
            
            if let c = colorForTitle[myTags[Int(index)]] {
                token.backgroundColor = c
            }
            return token
        }
        
        return nil
    }
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int) {
        self.tokenView(tokenView, didDeleteTokenAtIndex: index)
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int) {}
    

    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int) {
        if index < self.myTags.count{
            self.myTags.remove(at: Int(index))
            
            
            tokenView.reloadData()
            //tokenView.layoutIfNeeded()
           
            
            //tokenView.textView.becomeFirstResponder()
            
            
            if tokenView.textView.isEditable {
                addTagButton.isHidden = true
            } else {
                addTagButton.isHidden = false
            }
        }
    }
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView) {
        tipsTableView.isHidden = false
        menuTopConstraint.constant = -107
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        
        
        topImageView.isHidden = true
        topSegmentedControl.isHidden = true
    }
    
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView) {
        self.tokenView(tokenView, didChangeText: tokenView.textView.text +  " ")
        
        tokenView.textView.isEditable = false
        addTagButton.isHidden = false
        
        tipsTableView.isHidden = true
        
        topImageView.isHidden = false
        topSegmentedControl.isHidden = false
        
        openDrawer()
    }
    
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String) {
        if text.replacingOccurrences(of: " ", with: "").characters.count > 0{
            let lastChar = text[text.characters.index(before: text.endIndex)]
            if lastChar == " "{
                
                let newTag = self.tokenView.textView.text.trimmingCharacters(in: CharacterSet.whitespaces)
               
                if myTags.count < 10{
                    self.myTags.append(newTag)
                }
                
                
                self.tokenView.textView.text = ""
                self.tokenView.reloadData()
            }
        }
    }
    
    
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String) {
        //return button pressed
        self.tokenViewDidEndEditing(tokenView)
        tokenView.resignFirstResponder()
    }
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize) {
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
        self.view.layoutIfNeeded()
        setNewFrameAddTagButton()
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int) {
    }
}

extension SearchViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        menuTopConstraint.constant = handleHeight - tokenContainer.frame.height
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })
        drawerIsOpen = false

        if marker.iconView != nil {
            if (marker.iconView?.isKind(of: EventPin.self))! {
                let pinView = marker.iconView as! EventPin
                pinView.selectedImageView.isHidden = !pinView.selectedImageView.isHidden
                mapSearchView.eventOneLineCollectionView.isHidden = false
                
                UIView.animate(withDuration: 0.5, animations: {
                    self.mapSearchView.setStateForCircleButtons(isTopState: true)
                })
                
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print(coordinate)
        if isAddPinMode {
            let marker = GMSMarker(position: coordinate)
            marker.map = mapSearchView
            marker.icon = #imageLiteral(resourceName: "flagPin")
            
            isAddPinMode = false
            mapSearchView.flagButton.isSelected = false
            mapSearchView.flagButton.layer.shadowColor = UIColor.black.cgColor
            mapSearchView.eventOneLineCollectionView.isHidden = false
        }
    }
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        endEditTags()
    }
}

extension SearchViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView != tipsTableView{
         //   endEditTags()
        }
    }
}

extension UIPanGestureRecognizer {
    func wentUp(theViewYouArePassing: UIView) -> Bool {
        return velocity(in: theViewYouArePassing).y < 0
    }
}

extension SearchViewController : SearchMapViewDelegate {
    func didPressFlagButton(state: Bool) {
        isAddPinMode = state
    }
}

extension SearchViewController : SearchSortedViewDeleagte {
    func didRemoveView() {
        self.searchBarButton.isSelected = false
    }
}
