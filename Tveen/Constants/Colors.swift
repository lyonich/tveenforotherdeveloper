//
//  Colors.swift
//  Tveen
//
//  Created by Lyonich on 24.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

struct Colors {
    static let violet = UIColor(colorLiteralRed: 156/255, green: 101/255, blue: 245/255, alpha: 1)
    static let historyDelete = UIColor(colorLiteralRed: 180/255, green: 49/255, blue: 49/255, alpha: 1)
    static let searchGrayColor = UIColor(red: 243/255, green: 243/255, blue: 244/255, alpha: 1.0)
    static let tableViewSectionTitleColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 1.0)
    
    static let gradientRed = UIColor(red: 241/255, green: 60/255, blue: 44/255, alpha: 1)
    static let gradientPink = UIColor(red: 212/255, green: 44/255, blue: 102/255, alpha: 1)
}
