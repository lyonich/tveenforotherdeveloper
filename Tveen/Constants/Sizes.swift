//
//  Sizes.swift
//  Tveen
//
//  Created by Lyonich on 25.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

struct Size {
  struct Display {
    static let width  = UIScreen.main.bounds.width
    static let height = UIScreen.main.bounds.height
  }
  
  struct MapBottomCircleButton {
    static let bottomOffset:CGFloat = 160.0
    static let width:CGFloat = 51.0
  }
  
}



