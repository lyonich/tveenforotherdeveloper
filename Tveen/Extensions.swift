//
//  Extensions.swift
//  Tveen Real Estate
//
//  Created by Kirill Dobryakov on 29/01/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

//  Extensions.swift
//  Tunel
//
//  Created by Kirill Dobryakov on 05/09/15.
//  Copyright (c) 2015 Kirill Dobryakov. All rights reserved.
//

import Foundation
import UIKit

//public func printFonts() {
//    let fontFamilyNames = UIFont.familyNames
//    for familyName in fontFamilyNames {
//        print("------------------------------")
//        print("Font Family Name = [\(familyName)]")
//        let names = UIFont.fontNames(forFamilyName: familyName )
//        print("Font Names = [\(names)]")
//    }
//}

public func isValidPhone(_ value: String) -> Bool {
    do {
        let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
        let matches = detector.matches(in: value, options: [], range: NSMakeRange(0, value.characters.count))
        if let res = matches.first {
            return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == value.characters.count && (value.characters.count == 11 && value.characters.first != "+" || (value.characters.count == 12 && value.characters.first == "+"))
        } else { return false }
    } catch { return false}
}

public func isValidEmail(_ testStr:String) -> Bool{
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: testStr)
}

public func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
}

extension UIColor{
    
    class func tveenRedColor() -> UIColor{
        return UIColor(red: 240.0/255, green: 60.0/255, blue: 44.0/255, alpha: 1.0)
    }
    
    class func veryCustomViewBackgroundColor() -> UIColor{
        return UIColor.myBlackColor()
    }
    
    class func myBlackColor() -> UIColor{
        return UIColor(red: 35.0/255, green: 35.0/255, blue: 35.0/255, alpha: 1.0)
    }
    
    class func borderColor() -> UIColor{
        return UIColor.placeholderColor()
    }
    
    class func progress1Color() -> UIColor{
        return UIColor(red: 36.0/255, green: 188.0/255, blue: 130.0/255, alpha: 1.0)
    }
    
    class func progress2Color() -> UIColor{
        return UIColor(red: 255.0/255, green: 224.0/255, blue: 0, alpha: 1.0)
    }
    
    class func progress3Color() -> UIColor{
        return UIColor(red: 1.0, green: 0, blue: 67.0/255, alpha: 1.0)
    }
    
    class func stationGreenColor() -> UIColor{
        return UIColor(red: 47.0/255, green: 151.0/255, blue: 47.0/255, alpha: 1.0)
    }
    
    class func stationRedColor() -> UIColor{
        return UIColor(red: 242.0/255, green: 0, blue: 23.0/255, alpha: 1.0)
    }
    
    class func stationOrangeColor() -> UIColor{
        return UIColor(red: 1.0, green: 130.0/255, blue: 3.0/255, alpha: 1.0)
    }
    
    class func stationBlueColor() -> UIColor{
        return UIColor(red: 43.0/255, green: 89.0/255, blue: 166.0/255, alpha: 1.0)
    }
    
    class func stationYellowColor() -> UIColor{
        return UIColor(red: 242.0/255, green: 214.0/255, blue: 1.0/255, alpha: 1.0)
    }
    
    class func stationBrownColor() -> UIColor{
        return UIColor(red: 115.0/255, green: 58.0/255, blue: 6.0/255, alpha: 1.0)
    }
    
    class func stationPurpleColor() -> UIColor{
        return UIColor(red: 158.0/255, green: 18.0/255, blue: 133.0/255, alpha: 1.0)
    }
    
    class func stationGrayColor() -> UIColor{
        return UIColor(red: 159.0/255, green: 152.0/255, blue: 144.0/255, alpha: 1.0)
    }
    
    class func placeholderColor() -> UIColor{
        return UIColor(red: 127.0/255, green: 127.0/255, blue: 127.0/255, alpha: 1.0)
    }
}

//public func <(a: Date, b: Date) -> Bool {
//    return a.compare(b) == ComparisonResult.orderedAscending
//}
//
//public func ==(a: Date, b: Date) -> Bool {
//    return a.compare(b) == ComparisonResult.orderedSame
//}

extension UIColor{
    
    static func randomColor() -> UIColor{
        let r = CGFloat.random()
        let g = CGFloat.random()
        let b = CGFloat.random()
        return UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    static func TveenPlaceholderColor() -> UIColor{
        return UIColor(red: 199/255, green: 199/255, blue: 199/255, alpha: 1.0)
    }
}

extension CGFloat{
    static func random() -> CGFloat{
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}




extension String {
    
    func isEmpty() -> Bool{
        return  !(self.replacingOccurrences(of: " ", with: "").characters.count > 0)
    }
    
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    func md5() -> String {
        let messageData = self.data(using:String.Encoding.utf8)
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData!.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData!.count), digestBytes)
            }
        }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func toDate(format: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self)!
    }
}

extension Date{
    func toString(format: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}



extension UIButton{
    func setBackgroundColor(_ color: UIColor, forState: UIControlState){
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()?.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        self.setBackgroundImage(colorImage, for: forState)
    }
}

extension UIView {
    func maskBottomPixels(_ pixelCount: CGFloat) {
        
        let maskLayer = CALayer()
        let circleLayer = CAShapeLayer()
        let circlePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 300, height: frame.height - pixelCount))
        circleLayer.frame = CGRect(origin: CGPoint.zero, size: frame.size)
        circleLayer.path = circlePath.cgPath
        circleLayer.fillColor = UIColor.black.cgColor
        maskLayer.frame = bounds
        maskLayer.addSublayer(circleLayer)
        
        print(frame.width)
        
        layer.mask = maskLayer
    }
}

