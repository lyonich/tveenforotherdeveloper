//
//  TveenButton.swift
//  Tveen Real Estate
//
//  Created by Kirill Dobryakov on 03/02/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

@IBDesignable class TveenButton: UIButton {
    
    var backView: UIView?
    
    @IBInspectable var textAlignment: String = "Center"{
        didSet{
            switch textAlignment{
            case "Left": contentHorizontalAlignment = UIControlContentHorizontalAlignment.left
            case "Right": contentHorizontalAlignment = UIControlContentHorizontalAlignment.right
            default: contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
            }
        }
    }
    
    @IBInspectable var imageColor: UIColor?{
        didSet{
            let origImage = image(for: UIControlState())
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            setImage(tintedImage, for: UIControlState())
            tintColor = imageColor
        }
    }
    
    @IBInspectable var opacity: CGFloat = 255{
        didSet{
            backgroundColor = backgroundColor?.withAlphaComponent(opacity/255)
        }
    }
    
    @IBInspectable var maskPixels: CGFloat = 0{
        didSet{
            maskBottomPixels(maskPixels)
        }
    }
    
    @IBInspectable var highlightedColor: UIColor = UIColor.white{
        didSet{
            setBackgroundColor(highlightedColor.withAlphaComponent(1), forState: .highlighted)
            setBackgroundColor(highlightedColor.withAlphaComponent(1), forState: .selected)
        }
    }
    
    @IBInspectable var disabledColor: UIColor?{
        didSet{
            setBackgroundColor(disabledColor!, forState: .disabled)
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor?{
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
}
