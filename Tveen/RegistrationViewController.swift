//
//  RegistrationViewController.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 11/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import DigitsKit

enum TypeRegistration {
    case Login
    case VK
    case FB
    case OK
}

protocol RegistrationRouterDelegate {
    func closeRegistration()
    func showConfirmationPhone(registrationModel:LoginModel)
    func showChangeCategory(registrationModel:LoginModel)
}

class RegistrationViewController: UIViewController {
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var login: TveenTextField!
    @IBOutlet weak var passField: TveenTextField!
    @IBOutlet weak var passConfirmField: TveenTextField!
    @IBOutlet weak var birthday: TveenTextField!
    @IBOutlet weak var phoneField: TveenTextField!
    @IBOutlet weak var registerButton: TveenButton!
    
    @IBOutlet weak var pickerBottom: NSLayoutConstraint!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    
    var typeRegistration: TypeRegistration = .Login
    
    var registrationModel: LoginModel?
    
    var routerDelegate: RegistrationRouterDelegate!
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        addRedPinkGradient(view: self.view)
        
//        birthday.delete = self
//        phoneField.field.delegate = self
        passField.field.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
        passConfirmField.field.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
        phoneField.field.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(finishEditing)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool){
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func registration() {
//        routerDelegate.showConfirmationPhone(registrationModel: LoginModel())
        
        if passField.text == passConfirmField.text {
            let digits = Digits.sharedInstance()
            digits.logOut()

            let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
            configuration?.phoneNumber = self.phoneField.text!
            digits.authenticate(with: nil, configuration: configuration!) { session, error in
                if error == nil {
                    AuthorizationManager.shared.registration(login: self.login.text!, password: self.passField.text!, phone: self.phoneField.text!, birthday: self.birthday.datePicker!.picker.date.toString(format: Dates.serverDateFormat), success: { (data) in
                        self.spinner(isShow: false)
                        self.registrationModel = data
                        
                        self.routerDelegate.showChangeCategory(registrationModel: self.registrationModel!)
                    }, failure: { (error) in
                        self.spinner(isShow: false)
                        
                        self.showAlert(title: "Ошибка!", message: error.domain)
                    })
                } else {
                    self.spinner(isShow: false)
                }
            }
        } else {
            self.showAlert(title: "Ошибка!", message: "Введенные пароли не совпадают")
        }
        
        
        spinner(isShow: true)
        

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        let button = UIBarButtonItem()
        button.title = ""
        button.style = .plain
        navigationItem.backBarButtonItem = button
        
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }
    
    func showAlert(_ title: String){
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: {(alert :UIAlertAction!) in
            alertController.dismiss(animated: true, completion: nil)
            self.registerButton.isHidden = false
            self.spinner.stopAnimating()
        })
        alertController.addAction(deleteAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func regPressed(_ sender: AnyObject) {
        if login.text == "" || passField.text == "" || passConfirmField.text == "" || birthday.text == "" || phoneField.text == "" {
            showAlert(title: "Ошибка", message: "Не все поля заполнены")
//            registrationModel = LoginModel()
//            registrationModel?.login = login.text!
//            registrationModel?.password = passField.text!
//            registrationModel?.birthday = birthday.text!
//            registrationModel?.phone = phoneField.text!
        } else {
            self.registration()
        }
    }
    
    @IBAction func haveAccountPressed(_ sender: AnyObject){
        routerDelegate.closeRegistration()
    }
    
    func valueChanged(){
        registerButton.isHidden = false
        spinner.stopAnimating()
        
        phoneField.valid = isValidPhone(phoneField.text!)
        passConfirmField.valid = passField.text == passConfirmField.text && passField.text != ""
        registerButton.isHidden = !phoneField.valid && !passConfirmField.valid
    }
    
    func finishEditing(){
        hidePickers()
        
        view.endEditing(true)
        bottom.constant = 0
        layout()
    }
    
    func hidePickers(){
        for v in containerView.subviews{
            if let f = v as? TveenTextField{
                f.hidePicker()
            }
        }
    }
    
    func keyboardWillShow(_ notification: Notification){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            bottom.constant = keyboardSize.height
            layout()
        }
    }
    
    func keyboardWillHide(_ notification: Notification){
        bottom.constant = 0
        layout()
    }
    
    func layout(){
        UIView.animate(withDuration: 0.2, animations: {self.view.layoutIfNeeded()})
    }
    
    //MARK: - Private
    
    private func spinner(isShow:Bool) {
        if isShow == true {
            self.registerButton.isHidden = true
            self.spinner.startAnimating()
        } else {
            self.registerButton.isHidden = false
            self.spinner.stopAnimating()
        }
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneField.field {
            textField.text = "+7"
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == phoneField.field {
            if textField.text == "+7" {
                textField.text = ""
            }
        }
    }
    
}


//func makeTheme() {
//    let theme = DGTAppearance()
//    theme.bodyFont = UIFont(name: "SFUIText-Regular", size: 16)
//    theme.labelFont = UIFont(name: "SFUIText-Regular", size: 17)
//    theme.accentColor = UIColor.tveenRedColor()
//    theme.backgroundColor = UIColor.white
//    theme.logoImage = UIImage(named: "tveenLogo.PNG")
//}


