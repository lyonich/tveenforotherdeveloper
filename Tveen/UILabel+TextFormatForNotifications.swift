//
//  UILabel+TextFormatForNotifications.swift
//  Tveen
//
//  Created by Lyonich on 24.02.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

extension UILabel {
    func setFormatText(news: String, date: String) {
        var fontFinal = UIFont()
        
        if let font = UIFont(name: "SFUIText-Regular", size: 15) {
            fontFinal = font
        }
        
        let newsAttributs = [ NSFontAttributeName: fontFinal, NSForegroundColorAttributeName: UIColor.black] as [String : Any]
        let newsTextAttributed = NSAttributedString(string: news, attributes: newsAttributs)
        
        let dateAttributs = [ NSFontAttributeName: fontFinal, NSForegroundColorAttributeName: UIColor.gray] as [String : Any]
        let dateTextAttributed = NSAttributedString(string: date, attributes: dateAttributs)
        
        let finalText = NSMutableAttributedString()
        finalText.append(newsTextAttributed)
        finalText.append(dateTextAttributed)
        
        self.attributedText = finalText
    }
}
