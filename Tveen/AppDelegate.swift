//
//  AppDelegate.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 11/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import SplunkMint
import GoogleMaps
import GooglePlaces
import Crashlytics
import Fabric
import DigitsKit
import FBSDKLoginKit
import VK_ios_sdk
import TwitterKit

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coordinator = Coordinator.sharedInstance
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool{
        
        Mint.sharedInstance().disableNetworkMonitoring()
        Mint.sharedInstance().initAndStartSession(withAPIKey: "591837c9")
        
        Fabric.with([Digits.self, Twitter.self, Crashlytics.self])

        UINavigationBar.appearance().barTintColor = UIColor.tveenRedColor()
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        UINavigationBar.appearance().backgroundColor = UIColor.clear
        UINavigationBar.appearance().isOpaque = true
        
        prepareWindow()
        
        coordinator.startCoordinatorWithWindow(window!)
        
        GMSServices.provideAPIKey("AIzaSyCMgoqMRbm0ywm1QwoOAdEqlj_r-95wGEo")
        GMSPlacesClient.provideAPIKey("AIzaSyCMgoqMRbm0ywm1QwoOAdEqlj_r-95wGEo")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        let fb = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        VKSdk.processOpen(url, fromApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String!)
        
        if Twitter.sharedInstance().application(app, open:url, options: options) {
            return true
        }
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    //MARK: - Prviate
    
    private func prepareWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window!.backgroundColor = UIColor.white
        window!.makeKeyAndVisible()
    }
    
    
}

