//
//  SearchCategoriesTableView.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 21/08/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import NWSTokenView


class SearchCategoryViewController: UIViewController {
    
    @IBOutlet weak var tipsTableView: UITableView!
    
    @IBOutlet weak var searchBarTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tokenView: NWSTokenView!
    @IBOutlet weak var tokenViewHeightConstraint: NSLayoutConstraint!
    var selectedCategories = [false, false, false, false, false, false, false]
    
    var myTags = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    let categoryNames = ["Развлечения и отдых", "Ночная жизнь", "Спорт", "Искусство", "Образование и бизнес", "Для детей", "Другое"]
    let categoryImages = ["c1.png","c6.png","c4.png","c5.png","c7.png","c3.png","c2.png"]
    
    let tipPlaces = ["ул. Вешняковского", "пр. Вернадского"]
    let tipTags = ["Вечеринка", "Велосипед", "Вермишель"]
    
    let tokenViewMinHeight: CGFloat = 20
    let tokenViewMaxHeight: CGFloat = 150.0
    
    var colorForTitle = [String: UIColor]()

    override func viewDidLoad(){
        super.viewDidLoad()
        
        setupNavigationController()
        
        tokenView.dataSource = self
        tokenView.delegate = self
        tokenView.reloadData()
        
       // tokenView.textView.returnKeyType = .done
        tokenView.textView.returnKeyType = .search
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool){
        
        tokenView.reloadData()
        tokenView.layoutSubviews()
        view.layoutIfNeeded()
    }
    
    func setupNavigationController(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Далее", style: .done, target: self, action:#selector(nextScreen))
        
//        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "burger"), landscapeImagePhone: #imageLiteral(resourceName: "burger"), style: .plain, target: self, action: #selector(openMenu))
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 480, height: 44))
        label.numberOfLines = 2
        label.text =  "Выберите категории"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont(name: "SFUIText-Regular", size: 18)
        
        navigationItem.titleView = label
    }
    
    @IBOutlet weak var tokenViewTopConstraint: NSLayoutConstraint!
    func nextScreen(){
        performSegue(withIdentifier: "next", sender: self)
    }
    
    func cancel(){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
    
    tokenView.endEditing(true)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        navigationController?.setNavigationBarHidden(true, animated: true)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            tokenViewTopConstraint.constant = 24
             searchBarTrailingConstraint.constant = 8 + 60
            tableBottomConstraint.constant = keyboardSize.height
            UIView.animate(withDuration: 0.2, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillHide(_ notification: Notification){
        
         navigationController?.setNavigationBarHidden(false, animated: true)
        
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil{
            tokenViewTopConstraint.constant = 8
            searchBarTrailingConstraint.constant = 8
            tableBottomConstraint.constant = 0
            UIView.animate(withDuration: 0.2, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let button = UIBarButtonItem()
        button.title = ""
        button.style = .plain
        navigationItem.backBarButtonItem = button
        
        (segue.destination as! SearchViewController).selectedCategories = selectedCategories
        (segue.destination as! SearchViewController).colorForTitle = colorForTitle
        (segue.destination as! SearchViewController).myTags = myTags
        
        self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }
    
    
    
}

extension SearchCategoryViewController: UITableViewDelegate, UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tipsTableView{
            return 2
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == tipsTableView{
            return section == 0 ? "Места" : "Теги"
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if tableView == tipsTableView{
            return section == 0 ? 2 : 3
        }
        else{
            
            return categoryNames.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if tableView == tipsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tipsCell", for: indexPath) as! TipsCell
            cell.tipLabel.text = indexPath.section == 0 ? tipPlaces[indexPath.row] : tipTags[indexPath.row]
            cell.tipImageView.image =  indexPath.section == 0 ? #imageLiteral(resourceName: "pinForTips") : #imageLiteral(resourceName: "#")
            return cell
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCategoryCell", for: indexPath) as! SearchCategoriesCell
            
            cell.name.text = categoryNames[indexPath.row]
            cell.im.image = UIImage(named: categoryImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
            cell.im.tintColor = UIColor.gray
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == tipsTableView ? 50 : 76
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tipsTableView{
            if indexPath.section == 0{
                self.tokenView.textView.text = tipPlaces[indexPath.row] + " "
                self.tokenView(tokenView, didChangeText: tipPlaces[indexPath.row] + " ")
                tableView.deselectRow(at: indexPath, animated: true)
                
                tokenView.tokenForIndex(myTags.count - 1).backgroundColor = UIColor(red: 156/255, green: 101/255, blue: 245/255, alpha: 1)
                colorForTitle[tipPlaces[indexPath.row]] = UIColor(red: 156/255, green: 101/255, blue: 245/255, alpha: 1)
                
            }
            else{
                self.tokenView.textView.text = tipTags[indexPath.row] + " "
                self.tokenView(tokenView, didChangeText: tipTags[indexPath.row] + " ")
                tableView.deselectRow(at: indexPath, animated: true)
                
            }
        }
        else{
            (tableView.cellForRow(at: indexPath) as! SearchCategoriesCell).im.image = UIImage(named: categoryImages[indexPath.row])?.withRenderingMode(.alwaysOriginal)
            
            (tableView.cellForRow(at: indexPath) as! SearchCategoriesCell).name.textColor = UIColor.black
            
            selectedCategories[indexPath.row] = true
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        (tableView.cellForRow(at: indexPath) as! SearchCategoriesCell).name.textColor = UIColor.lightGray
        (tableView.cellForRow(at: indexPath) as! SearchCategoriesCell).im.image = UIImage(named: categoryImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        

        selectedCategories[indexPath.row] = true
    }
    
}

extension SearchCategoryViewController: NWSTokenDataSource, NWSTokenDelegate{
    
    // MARK: NWSTokenDataSource
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int{
        return myTags.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets?{
        return UIEdgeInsetsMake(3, 3, 0, 0)
        
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String?{
        return "      "
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String?{
        return "Введите название, тег или место"
    }

    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView?{
        if let token = NWSImageToken.initWithTitle(myTags[Int(index)], image: nil){
            
            if let c = colorForTitle[myTags[Int(index)]]{
                token.backgroundColor = c
            }
            return token
        }
        
        return nil
    }
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int){
        self.tokenView(tokenView, didDeleteTokenAtIndex: index)
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int){}
    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int){
        if index < self.myTags.count{
            self.myTags.remove(at: Int(index))
            
            tokenView.reloadData()
        }
    }
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView){
        tipsTableView.isHidden = false
      //  menuTopConstraint.constant = -107
        UIView.animate(withDuration: 0.2, animations: {
            self.view.layoutIfNeeded()
        })

    }
    
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView) {
       
        
        if tokenView.textView.text != "Введите название, тег или место"{
            self.tokenView(tokenView, didChangeText: tokenView.textView.text +  " ")
            
        
        }
        
        tipsTableView.isHidden = true
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String){
        
        if text != "Введите название, тег или место"{
        
        
        if text.replacingOccurrences(of: " ", with: "").characters.count > 0{
            let lastChar = text[text.characters.index(before: text.endIndex)]
            if lastChar == " "{
                
                let newTag = self.tokenView.textView.text.trimmingCharacters(in: CharacterSet.whitespaces)
                
                
                if myTags.count < 10{
                    self.myTags.append(newTag)
                }
                
                self.tokenView.textView.text = ""
                self.tokenView.reloadData()
            }
        }
        }
    }
    
    
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String){
        self.tokenViewDidEndEditing(tokenView)
        tokenView.resignFirstResponder()
        tokenView.textView.resignFirstResponder()
        
    }
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize){
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
        self.view.layoutIfNeeded()
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int){
    }
}

