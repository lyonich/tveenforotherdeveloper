//
//  TokenManager.swift
//  Tveen
//
//  Created by Lyonich on 16.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit


class TokenManager {
    
    static let sharedInstance = TokenManager()
    
    var tokenSaved: String?
    
    func saveToken(_ token: String) {
        tokenSaved = token
//        UserDefaults.standard.set(token, forKey: "token")
//        UserDefaults.standard.synchronize()
    }
    
    func token() -> [String:String] {
        let token = tokenSaved
        if let token = token {
            return ["X-Tween-Token-Session":token]
        }
        return ["X-Tween-Token-Session":""]
    }
}

