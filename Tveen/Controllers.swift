//
//  Controllers.swift
//  Tveen
//
//  Created by Lyonich on 15.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

struct Controllers {

    static let registration = UIStoryboard(name: "Registration", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
    static let login = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    
    static let confirmationPhone = UIStoryboard(name: "PhoneConfirmation", bundle: nil).instantiateViewController(withIdentifier: "PhoneConfirmationViewController") as! PhoneConfirmationViewController
    
    static let changeCategory = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
    
    static let searchViewController = UIStoryboard(name: "SearchCategoryView", bundle: nil).instantiateViewController(withIdentifier: "SearchnavigationController") as! UINavigationController

    
}
