//
//  UIHelper.swift
//  Tveen
//
//  Created by Lyonich on 10.02.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

func setCornerRadius(view: UIView, corners: UIRectCorner, radius: CGFloat) {
    let maskPath = UIBezierPath(roundedRect: view.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: CGSize(width: radius, height: radius))
    
    let shape = CAShapeLayer()
    shape.path = maskPath.cgPath
    view.layer.mask = shape
}


public func setCornerRadiusWithShadow(corners: UIRectCorner, radius: CGFloat, bounds: CGRect) -> CAShapeLayer
{
    let shadowLayer = CAShapeLayer()
    shadowLayer.path = UIBezierPath(roundedRect: bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius)).cgPath
    

    shadowLayer.fillColor = UIColor.white.cgColor
    
    shadowLayer.shadowColor = UIColor.darkGray.cgColor
    shadowLayer.shadowPath = shadowLayer.path
    shadowLayer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    shadowLayer.shadowOpacity = 0.8
    shadowLayer.shadowRadius = 2
    
    return shadowLayer
    
    //layer.insertSublayer(shadowLayer, below: nil) // also works
}
