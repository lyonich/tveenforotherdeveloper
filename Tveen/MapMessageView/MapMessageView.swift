//
//  MapMessageView.swift
//  Tveen
//
//  Created by Lyonich on 24.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

enum MapMessageViewState {
  case flagState
  case penState
  case myLocationState
}

class MapMessageView: UIView {
  
  @IBOutlet weak var darkBackgroundView: UIView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var imageView: UIImageView!

  override func awakeFromNib() {
    super.awakeFromNib()
    
    darkBackgroundView.layer.cornerRadius = 19
  }
  
  //MARK: - Public
  
  func setState(state: MapMessageViewState) {
    switch state {
    case MapMessageViewState.flagState:
      imageView.image = #imageLiteral(resourceName: "flagMapMessage")
      titleLabel.text =  "Какой-то пояснительный текст про пин"
    case MapMessageViewState.penState:
      imageView.image = #imageLiteral(resourceName: "penMessageMapImage")
      titleLabel.text =  "Нарисуйте область на карте для поиска"
    case MapMessageViewState.myLocationState:
      imageView.image = #imageLiteral(resourceName: "myLocationMapMessage")
      titleLabel.text =  "Обновляем информацию о локации..."
    }
  }

}
