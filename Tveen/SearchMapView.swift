//
//  SearchMapView.swift
//  Tveen
//
//  Created by Lyonich on 16.02.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit
import GoogleMaps

protocol SearchMapViewDelegate {
    func didPressFlagButton(state: Bool)
}

class SearchMapView: GMSMapView {
    
    @IBOutlet var flagButton: UIButton!
    @IBOutlet var penButton: UIButton!
    @IBOutlet var myLocationButton: UIButton!
    
    @IBOutlet weak var eventOneLineCollectionView: UICollectionView!
    
    var mapMessageView = MapMessageView.loadFromNibNamed(nibNamed: "MapMessageView") as! MapMessageView
    var delegateView: SearchMapViewDelegate!
    
    var locationManager = CLLocationManager()
    
    let myLocationPin = MyLocationPin.loadFromNibNamed(nibNamed: "MyLocationPin")
    var myLocationMarker = GMSMarker()

    override func awakeFromNib() {
        super.awakeFromNib()
        
        addSubview(flagButton)
        addSubview(penButton)
        addSubview(myLocationButton)
        
        setStateForCircleButtons(isTopState: false)
        addShadowForCircleButton()
        configurateOneLineCollectionView()
        createPins()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
        myLocationPin?.frame = CGRect(x: 0, y: 0, width: 33, height: 47)
        
    }
    
    override func layoutSubviews() {
        createMapMessageView()
        layoutIfNeeded()
    }
    
    //MARK: - Actions
    
    @IBAction func myLocationButtonAction(_ sender: UIButton) {
        mapMessageView.isHidden =  false
        mapMessageView.setState(state: .myLocationState)
        
        sender.isSelected = true
        flagButton.isSelected = false
        sender.layer.shadowColor = UIColor.red.cgColor
        mapMessageView.isHidden =  !sender.isSelected
        mapMessageView.setState(state: .myLocationState)
        flagButton.isSelected = !sender.isSelected
        penButton.isSelected = !sender.isSelected
        flagButton.layer.shadowColor = UIColor.black.cgColor
        penButton.layer.shadowColor = UIColor.black.cgColor
        
        locationManager.startUpdatingLocation()
    }
    
    @IBAction func flagButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        
        if sender.isSelected {
            sender.layer.shadowColor = UIColor.red.cgColor
            mapMessageView.isHidden =  !sender.isSelected
            mapMessageView.setState(state: .flagState)
            myLocationButton.isSelected = !sender.isSelected
            penButton.isSelected = !sender.isSelected
            penButton.layer.shadowColor = UIColor.black.cgColor
            myLocationButton.layer.shadowColor = UIColor.black.cgColor
            
            delegateView.didPressFlagButton(state: true)
        } else {
            sender.layer.shadowColor = UIColor.black.cgColor
            mapMessageView.isHidden =  !sender.isSelected
            
            delegateView.didPressFlagButton(state: false)
        }
    }
    
    @IBAction func penButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            sender.layer.shadowColor = Colors.violet.cgColor
            sender.layer.shadowOpacity = 1
            mapMessageView.isHidden =  !sender.isSelected
            self.flagButton.isSelected = !sender.isSelected
            self.myLocationButton.isSelected = !sender.isSelected
            self.flagButton.layer.shadowColor = UIColor.black.cgColor
            self.myLocationButton.layer.shadowColor = UIColor.black.cgColor
            mapMessageView.setState(state: .penState)
        } else {
            sender.layer.shadowColor = UIColor.black.cgColor
            sender.layer.shadowOpacity = 0.5
            mapMessageView.isHidden =  !sender.isSelected
            self.flagButton.isSelected = false
        }
    }
    
    //MARK: - Public
    
    func setStateForCircleButtons(isTopState:Bool) {
        if isTopState {
            flagButton.frame = CGRect(x: 20, y: UIScreen.main.bounds.height - Size.MapBottomCircleButton.bottomOffset - 100, width: Size.MapBottomCircleButton.width, height: Size.MapBottomCircleButton.width)
            penButton.frame = CGRect(x: flagButton.frame.origin.x + 20 + Size.MapBottomCircleButton.width, y: UIScreen.main.bounds.height - Size.MapBottomCircleButton.bottomOffset - 100, width: Size.MapBottomCircleButton.width, height: Size.MapBottomCircleButton.width)
            myLocationButton.frame = CGRect(x: UIScreen.main.bounds.width - 20 - Size.MapBottomCircleButton.width, y: UIScreen.main.bounds.height - Size.MapBottomCircleButton.bottomOffset - 100, width: Size.MapBottomCircleButton.width, height: Size.MapBottomCircleButton.width)
        } else {
            flagButton.frame = CGRect(x: 20, y: UIScreen.main.bounds.height - Size.MapBottomCircleButton.bottomOffset, width: Size.MapBottomCircleButton.width, height: Size.MapBottomCircleButton.width)
            penButton.frame = CGRect(x: flagButton.frame.origin.x + 20 + Size.MapBottomCircleButton.width, y: UIScreen.main.bounds.height - Size.MapBottomCircleButton.bottomOffset, width: Size.MapBottomCircleButton.width, height: Size.MapBottomCircleButton.width)
            myLocationButton.frame = CGRect(x: UIScreen.main.bounds.width - 20 - Size.MapBottomCircleButton.width, y: UIScreen.main.bounds.height - Size.MapBottomCircleButton.bottomOffset, width: Size.MapBottomCircleButton.width, height: Size.MapBottomCircleButton.width)
        }
    }

    
    //MARK: - Private
    
    //WARNING: - Заглушка
    private func createPins() {
        var increaseValue = 0.0
        
        for i in 1 ... 15 {
            
            let  position = CLLocationCoordinate2DMake(55.75222, 37.61556 + increaseValue * 4)
            let marker = GMSMarker(position: position)
            marker.map = self
            
            increaseValue = increaseValue + 0.8
            
            let pin = EventPin.loadFromNibNamed(nibNamed: "EventPin") as! EventPin
            pin.frame = CGRect(x: 0, y: 0, width: 33, height: 46)
            
            switch i {
            case 1:
                
                pin.pinImageView.image = #imageLiteral(resourceName: "sportRedPin")
            case 2:
                pin.pinImageView.image = #imageLiteral(resourceName: "sportVioletPin")
            case 3:
                pin.pinImageView.image = #imageLiteral(resourceName: "lightRedPin")
            case 4:
                pin.pinImageView.image = #imageLiteral(resourceName: "lightVioletPin")
            case 5:
                pin.pinImageView.image = #imageLiteral(resourceName: "gameRedPin")
            case 6:
                pin.pinImageView.image = #imageLiteral(resourceName: "gameVioletPin")
            case 7:
                pin.pinImageView.image = #imageLiteral(resourceName: "nightRedPin")
            case 8:
                pin.pinImageView.image = #imageLiteral(resourceName: "nightVioletPin")
            case 9:
                pin.pinImageView.image = #imageLiteral(resourceName: "bookRedPin")
            case 10:
                pin.pinImageView.image = #imageLiteral(resourceName: "bookVioletPin")
            case 11:
                pin.pinImageView.image = #imageLiteral(resourceName: "cultureRedPin")
            case 12:
                pin.pinImageView.image = #imageLiteral(resourceName: "cultureVioletPin")
            case 13:
                pin.pinImageView.image = #imageLiteral(resourceName: "childRedPin")
            case 14:
                pin.pinImageView.image = #imageLiteral(resourceName: "childVioletPin")
            case 15:
                pin.pinImageView.image = #imageLiteral(resourceName: "flagPin")
            default:
                break
            }
            marker.iconView = pin
        }
    }
    
    private func configurateOneLineCollectionView() {
        eventOneLineCollectionView.frame = CGRect(x: 0, y: Size.Display.height - 8 - 97 - 64 - 30, width: Size.Display.width, height: 97)
        addSubview(eventOneLineCollectionView)
        
        
        eventOneLineCollectionView.dataSource = self
        eventOneLineCollectionView.delegate = self
        eventOneLineCollectionView.register(SearchEventCollectionCell.self, forCellWithReuseIdentifier: "SearchEventCollectionCell")
        eventOneLineCollectionView.register(UINib(nibName: "SearchEventCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SearchEventCollectionCell")
    }
    
    private func createMapMessageView() {
        addSubview(mapMessageView)
        mapMessageView.frame = CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 38)
        mapMessageView.isHidden = true
    }
    
    private func addShadowForCircleButton() {
        [flagButton, penButton, myLocationButton].forEach { (button) in
            button?.layer.shadowColor = UIColor.black.cgColor
            button?.layer.shadowOpacity = 0.5
            button?.layer.shadowRadius = 7
            button?.layer.shadowOffset = CGSize(width: 0, height: 1)
        }
    }
    
   
}

extension SearchMapView: UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchEventCollectionCell", for: indexPath) as! SearchEventCollectionCell
        switch indexPath.row {
        case 0:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "sportIndicator")
        case 1:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "childIndicator")
        case 2:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "nightIndicator")
        case 3:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "lightIndicator")
        case 4:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "gameIndicator")
        case 5:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "cultureIndicator")
        case 6:
            cell.typeIndicatorImageView.image = #imageLiteral(resourceName: "bookIndicator")
        default:
            break
        }
        
        return cell
    }
}

extension SearchMapView : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Size.Display.width, height: 97)
    }
}

extension SearchMapView : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        myLocationMarker.map = nil
        let location = locations.last
        
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude:(location?.coordinate.longitude)!, zoom:14)
        animate(to: camera)
        
        self.locationManager.stopUpdatingLocation()
        myLocationButton.layer.shadowColor = UIColor.black.cgColor
        myLocationButton.isSelected = false
        mapMessageView.isHidden =  true
        
        let  position = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
        myLocationMarker = GMSMarker(position: position)
        myLocationMarker.map = self
        myLocationMarker.iconView = myLocationPin
    }
}


extension SearchMapView: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scroll!!!")
    }
}
