//
//  PhoneConfirmationViewController.swift
//  Tveen
//
//  Created by Lyonich on 03.04.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit
import DigitsKit

protocol PhoneConfirmationControllerRouterDelegate {
    func backToParentController()
}

class PhoneConfirmationViewController: UIViewController {
    
    var routerDelegate: PhoneConfirmationControllerRouterDelegate!

    @IBOutlet weak var nextButton: TveenButton!
    @IBOutlet weak var codeTextField: PaddingTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRedPinkGradient(view: self.view)
        codeTextField.layer.cornerRadius = 30
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func backButtonAction(_ sender: Any) {
        routerDelegate.backToParentController()
        let digits = Digits.sharedInstance()
        digits.logOut()
    }
    @IBAction func nextButtonAction(_ sender: Any) {
        let digits = Digits.sharedInstance()
        let configuration = DGTAuthenticationConfiguration(accountFields: .defaultOptionMask)
        configuration?.phoneNumber = "+79281473047"
        digits.authenticate(with: nil, configuration: configuration!) { session, error in
            if error == nil {
                print("УСПЕШНЛ")
            }
        }
    }
}
