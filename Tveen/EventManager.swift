//
//  EventManager.swift
//  Tveen
//
//  Created by Lyonich on 16.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



enum TypeVoteEvent: Int {
    case go         = 1
    case maybeGo    = 2
    case dontGo     = 3
}

class EventManager: NSObject {
    
    typealias SuccessWithEventData = (Event) -> Void
    typealias SuccessWithGetEventData = ([Event]) -> Void
    
    static let sharedManager = EventManager()
    
    func createEvent(parameters: [String : Any],success: @escaping SuccessWithEventData, failure: @escaping FailureWithError ) {
    
        let event = ["event" : parameters]
        
        let finalEvent = convertParameterToServerFormat(parameters: event)
        
        Alamofire.request(APIUrls.event, method: .put, parameters: finalEvent, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
                success(self.parseEventResponse(json: json))
            case .failure( _):
                if response.response?.statusCode == 409 {
                    let error = NSError(domain: "Не удалось создать мероприятие", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 401 {
                    let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 404 || response.response?.statusCode == 500  {
                    let error = NSError(domain: "Внутренняя ошибка сервера", code: (response.response?.statusCode)!, userInfo: nil)
                                        failure(error)
                }
            }
        }
    }
    
    func getEvents(parameters: [String : Any],success: @escaping SuccessWithGetEventData, failure: @escaping FailureWithError ) {
        
        let parameters = parameters
        
//        let event = ["event" : parameters]
        
        let finalEvent = convertParameterToServerFormat(parameters: parameters)
        
        Alamofire.request(APIUrls.event, method: .post, parameters: finalEvent, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
            
            var events = [Event]()
            
            if json.array?.count != nil {
                for event in json.array! {
                    events.append(EventManager.sharedManager.parseEventResponse(json: event))
                }
            }
            
            success(events)
            case .failure( _):
                if response.response?.statusCode == 409 {
                    let error = NSError(domain: "Не удалось получить мероприятия", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 401 {
                    let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 404 || response.response?.statusCode == 500  {
                    let error = NSError(domain: "Внутренняя ошибка сервера", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                }
            }
        }
    }
    
    func voteEvent(voteType: TypeVoteEvent, idEvent:String, success:@escaping (Any) -> Void, failure: @escaping FailureWithError ) {
        let url = APIUrls.event + idEvent + "/" + "vote/"
        
        let parameters = ["type":voteType.rawValue]
        let finalParameters = convertParameterToServerFormat(parameters: parameters)
        
        Alamofire.request(url, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            if response.response?.statusCode == 409 {
                let error = NSError(domain: "Пользователь уже голосовал", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 || response.response?.statusCode == 500  {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 200 {
                success("статус успешно изменен")
            } else if response.response?.statusCode == 423 {
                let error = NSError(domain: "Статус мероприятия закрыт", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }
            
        }
    }
    
    func editEvent(parameters: [String : Any], idEvent: String, success: @escaping SuccessWithEventData, failure: @escaping FailureWithError ) {
        let event = ["event" : parameters]
        let finalEvent = convertParameterToServerFormat(parameters: event)
        
        let url = APIUrls.event + idEvent + "/"
        
        Alamofire.request(url, method: .put, parameters: finalEvent, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
            success(self.parseEventResponse(json: json))
            case .failure( _):
                if response.response?.statusCode == 409 {
                    let error = NSError(domain: "Не удалось отредактировать мероприятие", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 401 {
                    let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 404 || response.response?.statusCode == 500  {
                    let error = NSError(domain: "Внутренняя ошибка сервера", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                }
            }
        }
    }
    
    func addContentToEvent(imageArray:[String], videoArray:[String], idEvent:String, success: @escaping SuccessWithEventData, failure: @escaping FailureWithError) {
        let url = APIUrls.event + idEvent + "/" + "content/"
        let parameters = ["images":imageArray, "video":videoArray]
        let finalParameters = convertParameterToServerFormat(parameters: parameters)
        
        Alamofire.request(url, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
            success(self.parseEventResponse(json: json))
            case .failure( _):
                if response.response?.statusCode == 409 {
                    let error = NSError(domain: "Не удалось отредактировать мероприятие", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 401 {
                    let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 404 || response.response?.statusCode == 500  {
                    let error = NSError(domain: "Внутренняя ошибка сервера", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 423 {
                    let error = NSError(domain: "Статус мероприятия закрыт", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 400 {
                    let error = NSError(domain: "Иденификатор не указан", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                }
            }
        }
    }
    
    
    func deleteMemberFromEvent(idEvent:String, memberId:String, success: @escaping SuccessWithEventData, failure: @escaping FailureWithError) {
        let url = APIUrls.event + idEvent + "/" + "member/" + memberId + "/"
        
        Alamofire.request(url, method: .put, parameters: nil, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
                success(self.parseEventResponse(json: json))
            case .failure( _):
                if response.response?.statusCode == 409 {
                    let error = NSError(domain: "Не удалось отредактировать мероприятие", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 401 {
                    let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 404 || response.response?.statusCode == 500  {
                    let error = NSError(domain: "Внутренняя ошибка сервера", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 423 {
                    let error = NSError(domain: "Статус мероприятия закрыт", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 400 {
                    let error = NSError(domain: "Иденификатор не указан", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                }
            }
        }

        
        
    }
    
    func parseEventResponse(json: JSON) -> Event {
        //TODO: Допарсить остальное
        let model = Event()
        
        
        
        
        print(json)
        
        if let id = json["info"]["id"].string {
            model.id = id
        }
        
        if let country = json["location"]["country"].string {
            model.country = country
        }
        if let region = json["location"]["region"].string {
            model.region = region
        }
        if let city = json["location"]["city"].string {
            model.city = city
        }
        
        if let coordinates = json["location"]["coordinates"].arrayObject as? [Double]{
            model.coordinates = CGPoint(x: coordinates[0], y: coordinates[1])
        }
        
        if let isPublicAccess: Bool = json["data_params"]["public_access"].bool {
            model.isPublicAccess = isPublicAccess
        }
        
        if let owner_uuid = json["info"]["owner_uuid"].string {
            model.owner_uuid = owner_uuid
        }
        
        if let title = json["info"]["title"].string {
            model.title = title
        }
        
        if let description = json["info"]["description"].string {
            model.description = description
        }
        
        if let type: Int = json["info"]["type"].int {
            model.type = type
        }
        
        if let category: Int = json["info"]["category"].int {
            model.category = Category(id: category)
        }
        
        if let mainImageURL: String = json["info"]["main_image"].string{
            model.mainImageURL = mainImageURL
        }
        
        if let ownerName = json["user"]["name"].string {
            model.ownerName = ownerName
        }
        
        if let ownerLogin = json["user"]["login"].string {
            model.ownerLogin = ownerLogin
        }
        
        if let ownerAvatarLink = json["user"]["avatar_url"].string {
            model.ownerAvatarLink = ownerAvatarLink
        }
        
        if let images = json["images"].arrayObject as? [String]{
            model.imageURLS = images
        }
        
        if let minAge = json["info"]["min_age"].int{
            model.minAge = minAge
        }
        if let maxAge = json["info"]["max_age"].int{
            model.maxAge = maxAge
        }
        
        if let minCount = json["info"]["min_count"].int{
            model.minCount = minCount
        }
        if let maxCount = json["info"]["max_count"].int{
            model.maxCount = maxCount
        }
        
        if let members = json["members"].arrayObject as? [String] {
            model.members = members
        }
        
        if let timeStart = json["time"]["start"].string{
            model.timeStart = timeStart
        }
        if let timeEnd = json["time"]["end"].string{
            model.timeEnd = timeEnd
        }
        if let timeDeadline = json["time"]["deadline"].string{
            model.timeDeadline = timeDeadline
        }
        if let tags = json["tags"].arrayObject as? [String]{
            model.tags = tags
        }
        
        return model
    }
}
