//
//  APIHelper.swift
//  Wilk Real Estate
//
//  Created by Kirill Dobryakov on 03/03/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class APIHelper{
    struct URLs{
        static let baseString = "http://52.41.159.42/api/"
        
        static let getToken         = baseString + "auth/app"
        static let register         = baseString + "auth/registration"
        static let login            = baseString + "auth/login"
        static let findPeople       = baseString + "findpeople"
        static let updateProfile    = baseString + "updateprofile"
        static let getProfile       = baseString + "getprofile"
        static let getAllProfiles   = baseString + "getallprofiles"
        static let getFeed          = baseString + "gettape"
        static let subscribe        = baseString + "friendship"
        static let checkFriendship  = baseString + "checkfriendship"
        static let unsubscribe      = baseString + "deletefriendship"
        static let createEvent      = baseString + "createevent"
        static let getEvent         = baseString + "getevent"
        static let getCreatedEvents = baseString + "geteventsbyid"
        static let getSubdEvents    = baseString + "geteventsbyidinvert"
        static let vote             = baseString + "vote"
        static let unVote           = baseString + "unvote"
        static func getFriends(type: PeopleType) -> String{
            switch type {
            case .friends:          return baseString + "getfriends"
            case .subscribers:      return baseString + "getsubscribers"
            case .subscriptions:    return baseString + "getsubscription"
            }
        }
    }
    
    
    static func getToken(){
        
//        let headers: HTTPHeaders = [
//            "111222333":"aaabbbccc",
//            "Accept": "application/json"
//        ]
//
//        Alamofire.request(URLs.getToken, headers: headers).responseJSON { response in
//            print(response.request!)
//            print(response.result.value!)
//        }
    }
    
    static func register(_ login: String, password: String, bDate: Date, phone: String, completion: @escaping (String, Profile?) -> Void){
        let parameters = ["login" : login, "password": password.md5(), "phone": phone, "bdate": bDate.formatDate()]
        
        Alamofire.request(URLs.register, parameters: parameters).responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                if json["status"] == "success"{
                    let me = Profile()
                    me.token = json["token"].string
                    me.id = json["id"].string
                    me.login = login
                    me.birthday = bDate
                    me.phone = phone
                    
                    completion("success", me)
                }
                else{
                    completion(json["status"].stringValue, nil)
                }
                
            case .failure(let error): print(error)
            }
        }
    }
    
    static func login(_ login: String, password: String, completion: @escaping (String, Profile?) -> Void){
        let parameters = ["login" : login, "password": password.md5()]
        
        Alamofire.request(URLs.login, parameters: parameters) .responseJSON { response in
            
            switch response.result {
            case .success(let value):
                
                let json = JSON(value)
                
                print(response.request!)
                print(response.result.value!)
                
                if json["status"] == "success"{
                    getProfile(userID: json["id"].stringValue, token: json["token"].stringValue, completion: { profile in
                        
                        profile.token = json["token"].string
                        completion("success", profile)
                    })
                }
                else{
                    completion(json["status"].stringValue, nil)
                }
                
            case .failure(let error): print(error)
            }
        }
    }
    
    static func findPeople(query: String, completion: @escaping ([ProfileSmall]?) -> Void){
        Alamofire.request(URLs.findPeople, parameters: ["string": query, "token": Profile.me.token!]) .responseJSON { response in
            
            print(query)
            //print("my request is \(response.request!)")
            //print(response.result.value!)
            
            var ps = [ProfileSmall]()
            
            if query.replacingOccurrences(of: " ", with: "") == ""{
                completion(ps)
            }
            else{
                if let JSON = response.result.value as? [[String: AnyObject]]{
                    for personItem in JSON{
                        let p = ProfileSmall()
                        p.name = personItem["name"] as? String
                        p.login = personItem["login"] as? String
                        p.id  = personItem["id"] as? String
                        ps.append(p)
                    }
                    completion(ps)
                }
                else { completion(nil) }
            }
        }
    }
    
    static func updateProfile(email: String?, place: String?, name: String?, info: String?, sex: String?, newImage: NSData?, completion: @escaping (String, String?) -> Void){
        var parameters: [String: String] = ["token": Profile.me.token!]
        
        parameters["name"] =  name
        parameters["about"] = info
        parameters["city"] =  place
        parameters["sex"] =   sex
        parameters["email"] = email
        
        let v =  Profile.me.birthday  != nil ? stringFrom(utcDate: Profile.me.birthday!) : "1998-05-07"
        parameters["bdate"] = v
        
        let myUrl = NSURL(string: URLs.updateProfile);
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";
        let boundary = "Boundary-\(NSUUID().uuidString)"
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.httpBody = createBodyWithParameters(parameters: parameters, filePathKey: "image", imageDataKey: newImage, boundary: boundary) as Data
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            
            if let data = data{
                let json = JSON(data: data)
                completion(json["status"].stringValue,json["profile_photo"].string)
            }
            else { completion("failure", nil) }
        }
        
        task.resume()
    }
    
    static func createBodyWithParameters(parameters: [String: String?]?, filePathKey: String?, imageDataKey: NSData?, boundary: String) -> NSData{
        let body = NSMutableData()
        
        if parameters != nil {
            for (key, value) in parameters! {
                if let v = value{
                    body.appendString(string: "--\(boundary)\r\n")
                    body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                    body.appendString(string: "\(v)\r\n")
                }
            }
        }
        
        let filename = "user-profile.jpg"
        let mimetype = "image/jpg"
        
        body.appendString(string: "--\(boundary)\r\n")
        
        if let imData = imageDataKey{
            body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
            body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
            body.append(imData as Data)
            body.appendString(string: "\r\n")
            body.appendString(string: "--\(boundary)--\r\n")
        }
        
        return body
    }
    
    static func getProfile(userID: String, token: String, completion: @escaping (Profile) -> Void){
        Alamofire.request(URLs.getProfile, parameters: ["user_id": userID, "token": token]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            if let INFO = response.result.value as? [String: AnyObject]{
                if INFO["id"] != nil{
                    let profile = Profile()
                    
                    if let bDate = INFO["bdate"] as? String{
                        if bDate != "0000-00-00"{
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            profile.birthday = dateFormatter.date(from: bDate)
                            let ageComponents = Calendar.current.dateComponents([.year], from: profile.birthday!, to: Date())
                            profile.age = String(ageComponents.year!)
                        }
                    }
                    
                    profile.login = INFO["login"] as? String
                    profile.name = INFO["name"] as? String
                    profile.id = INFO["id"] as? String
                    profile.info = INFO["about"] as? String
                    profile.place = INFO["city"] as? String
                    profile.phone = INFO["phone"] as? String
                    profile.rating = INFO["rating"] as? String
                    profile.sex = INFO["sex"] as? String
                    profile.email = INFO["email"] as? String
                    profile.friendsCount = (INFO["friendscount"] as! Int).description
                    profile.subscribersCount = (INFO["subscriberscount"] as! Int).description
                    profile.subscriptionCount = (INFO["subscriptioncount"] as! Int).description
                    profile.avatarURL = INFO["profile_photo"] as? String
                    
                    if profile.avatarURL != nil{
                        Alamofire.request("http://" + profile.avatarURL!) .responseJSON { response in
                            
                            profile.photo = response.data
                            completion(profile)
                        }
                    }
                    else{
                        completion(profile)
                    }
                }
            }
        }
    }
    
    static func getAllProfiles(completion: @escaping (Void) -> Void){
        Alamofire.request(URLs.getAllProfiles, parameters: ["token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            if let JSON = response.result.value as? [[String: AnyObject]]{
                Profile.profiles = [Profile]()
                
                for profileItem in JSON{
                    let profile = Profile()
                    
                    profile.id = profileItem["id"] as? String
                    profile.name = profileItem["name"] as? String
                    profile.login = profileItem["login"] as? String
                    profile.info = profileItem["info"] as? String
                    profile.rating = profileItem["rating"] as? String
                    profile.place = profileItem["place"] as? String
                    if let bDate = profileItem["bdate"] as? String{
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        profile.birthday = dateFormatter.date(from: bDate)
                        let ageComponents = Calendar.current.dateComponents([.year], from: profile.birthday!, to: Date())
                        profile.age = String(ageComponents.year!)
                    }
                    profile.sex = profileItem["sex"] as? String
                    profile.phone = profileItem["phone"] as? String
                    profile.email = profileItem["email"] as? String
                    
                    Profile.profiles.append(profile)
                }
                completion()
            }
        }
    }
    
    static func getFriends(userID: String, type: PeopleType, completion: @escaping ([ProfileSmall]?) -> Void){
        Alamofire.request(URLs.getFriends(type: type), parameters: ["user_id": userID, "token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            var people = [ProfileSmall]()
            
            if let JSON = response.result.value as? [[String: AnyObject]]{
                for personItem in JSON{
                    let p = ProfileSmall()
                    p.name = personItem["name"] as? String
                    p.login = personItem["login"] as? String
                    p.id  = personItem["id"] as? String
                    people.append(p)
                }
                completion(people)
            }
            else {completion(nil)}
        }
    }
    
    static func getFeed(completion: @escaping ([Event]?) -> Void){
        Alamofire.request(URLs.getFeed, parameters: [ "token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            if let JSON = response.result.value as? [[String: AnyObject]]{
                var feedEvents = [Event]()
                
                for eventItem in JSON{
                    let event = Event()
                    
                    event.name = eventItem["title"] as? String
                    event.category = eventItem["category"] as? String
                    event.id = eventItem["event_id"] as? String
                    event.about = eventItem["description"] as? String
                    event.date = stringFrom(utcDateString: eventItem["date"] as? String) + " " + stringFrom(utcTimeString: eventItem["time"] as? String)
                    event.isPrivate = eventItem["isprivate"] as? String == "1"
                    event.creatorID = eventItem["id"] as? String
                    event.status = eventItem["status"] as? String
                    event.tags = eventItem["tags"] as? [String]
                    event.creatorID = eventItem["id"] as? String
                    event.creatorLogin = eventItem["login"] as? String
                    event.creatorName = eventItem["name"] as? String
                    
                    feedEvents.append(event)
                }
                
                completion(feedEvents)
            }
            else {completion(nil)}
        }
    }
    
    static func checkFriendship(profileID: String, completion: @escaping (String?) -> Void){
        Alamofire.request(URLs.checkFriendship, parameters: ["friend_one_id": Profile.me.id!, "friend_two_id": profileID, "token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            switch response.result {
            case .success(let value): completion(JSON(value)["status"].string)
            case .failure(let error): print(error) }
        }
    }
    
    static func subscribe(profileID: String, completion: @escaping (Void) -> Void){
        Alamofire.request(URLs.subscribe, parameters: ["friend_one_id": Profile.me.id!, "friend_two_id": profileID, "token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            completion()
        }
    }
    
    static func unsubsribe(profileID: String, completion: @escaping (Void) -> Void){
        Alamofire.request(URLs.unsubscribe, parameters: ["friend_one_id": Profile.me.id!, "friend_two_id": profileID, "token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            completion()
        }
    }
    
    static func createEvent(event: Event, vc: UIViewController,completion: @escaping (String?) -> Void){
//        let parameters1: [String: String] =
//            [
//                "token": Profile.me.token!,
//                "id": Profile.me.id!,
//                "title": event.name!,
//                "description": event.about!,
//                "category": event.category!,
//                "tags": event.tagsString!,
//                "address": event.address!,
//                "position": event.position!,
//                "isprivate": event.isPrivate!.description
//        ]
//        
//        var parameters: [String: AnyObject] = parameters1 as [String : AnyObject]
//        
//        if let pMin = event.peopleMin{
//            if pMin.characters.count > 0{
//                parameters["min"] = pMin as AnyObject
//            }
//        }
//        if let pMax = event.peopleMax{
//            if pMax.characters.count > 0{
//                
//                parameters["max"] = pMax as AnyObject
//            }
//        }
//        if let aMin = event.ageMin{
//            if aMin.characters.count > 0{
//                parameters["minage"] = aMin as AnyObject
//            }
//        }
//        if let aMax = event.ageMax{
//            if aMax.characters.count > 0{
//                parameters["maxage"] = aMax as AnyObject
//            }
//        }
//        if let sDate = event.date{
//            if sDate.characters.count > 0{
//                parameters["date"] = sDate as AnyObject
//            }
//        }
//        if let sTime = event.time{
//            if sTime.characters.count > 0{
//                parameters["time"] = sTime as AnyObject
//            }
//        }
//        if let sDate = event.endDate{
//            if sDate.characters.count > 0{
//                
//                parameters["enddate"] = sDate as AnyObject
//            }
//        }
//        if let sTime = event.endTime{
//            if sTime.characters.count > 0{
//                
//                parameters["endtime"] = sTime as AnyObject
//            }
//        }
//        if let sDate = event.deadlineDate{
//            if sDate.characters.count > 0{
//                
//                parameters["deadlinedate"] = sDate as AnyObject
//            }
//        }
//        if let sTime = event.deadlineTime{
//            if sTime.characters.count > 0{
//                
//                parameters["deadlinetime"] = sTime as AnyObject
//            }
//        }
//        
//        
//        
//        parameters["party"] = event.party as AnyObject
//        
        
        if event.shouldPostFB{
            FBHelper.shared.postToWall(in: vc, popup: false, completion: { error in
                print("fb post complete. Error \(error)")
            })
        }
        
        if event.shouldPostVK{
            VKHelper.shared.postToWall(in: vc, popup: false, completion: { error in
                print("fb post complete. Error \(error)")
            })

        }
        
        completion("success" as! String?)
//        
//            Alamofire.request(URLs.createEvent, parameters: parameters) .responseJSON { response in
//            
//            print(response.result.value!)
//            
//            if let JSON = response.result.value as? [String: AnyObject]{
//                
//                
//                if JSON["status"] as! String? == "success"{
//                    
//
//                
//                }
//                
//                completion(JSON["status"] as! String?)
//            }
//            else { completion(nil) }
//        }
    }
    
    static func getEvent(eventID: String, completion: @escaping (Event?) -> Void){
        Alamofire.request(URLs.getEvent, parameters: ["event_id": eventID, "token": Profile.me.token!]) .responseJSON { response in
            
            print(response.request!)
            print(response.result.value!)
            
            if let eventItem = response.result.value as? [String: AnyObject]{
                let event = Event()
                
                event.name = eventItem["title"] as? String
                event.category = eventItem["category"] as? String
                event.id = eventItem["event_id"] as? String
                event.about = eventItem["description"] as? String
                event.date = stringFrom(utcDateString: eventItem["date"] as? String) + " " + stringFrom(utcTimeString: eventItem["time"] as? String)
                event.isPrivate = eventItem["isprivate"] as? String == "1"
                event.creatorID = eventItem["id"] as? String
                event.status = eventItem["status"] as? String
                event.tags = eventItem["tags"] as? [String]
                event.address = eventItem["address"] as? String
                event.peopleMax = eventItem["max"] as? String
                event.peopleMin = eventItem["min"] as? String
                event.ageMax = eventItem["maxage"] as? String
                event.ageMin = eventItem["minage"] as? String
                event.rating =  eventItem["rate"] as? String
                event.yesGo = eventItem["yes"] as? [String]
                event.noGo = eventItem["no"] as? [String]
                event.maybeGO = eventItem["maybe"] as? [String]
                event.creatorID = eventItem["id"] as? String
                event.creatorLogin = eventItem["login"] as? String
                event.creatorName = eventItem["name"] as? String
                
                event.deadlineDate = stringFrom(utcDateString: eventItem["deadlinedate"] as? String) + " " + stringFrom(utcTimeString: eventItem["deadlinetime"] as? String)
                event.endDate = stringFrom(utcDateString: eventItem["enddate"] as? String) + " " + stringFrom(utcTimeString: eventItem["endtime"] as? String)
                
                if let pos = eventItem["position"] as? [String]{
                    if pos.count == 2{
                        event.latitude = pos[0]
                        event.longitude = pos[1]
                    }
                }
                
                completion(event)
            }
            else { completion(nil) }
        }
    }
    
    static func stringFrom(utcDate: Date) -> String{
        let f = DateFormatter()
        f.dateFormat = "yyyy-MM-dd"
        return f.string(from: utcDate)
    }
    
    static func stringFrom(utcDateString: String?) -> String{
        
        if let s = utcDateString{
            let f = DateFormatter()
            f.dateFormat = "yyyy-MM-dd"
            f.timeZone = TimeZone(abbreviation: "GMT")
            
            let date = f.date(from: s)
            f.timeZone = TimeZone.current
            f.dateFormat = "dd MMMM"
            return f.string(from: date!)
        }
        else{
            return ""
        }
    }
    
    static func stringFrom(utcTimeString: String?) -> String{
        
        if let s = utcTimeString{
            let f = DateFormatter()
            f.dateFormat = "HH:mm:ss"
            f.timeZone = TimeZone(abbreviation: "GMT")
            
            let date = f.date(from: s)
            f.timeZone = TimeZone.current
            f.dateFormat = "HH:mm"
            return f.string(from: date!)
        }
        else{
            return ""
        }
    }
    
    //true - created
    //false - subscribed
    
    static func getEvents(created: Bool, profileID: String, completion: @escaping ([Event]?) -> Void){
        Alamofire.request(URLs.getCreatedEvents, parameters: ["user_id": profileID, "token": Profile.me.token!]) .responseJSON { response in
            
            if let JSON = response.result.value as? [[String: AnyObject]]{
                var events = [Event]()
                
                for eventItem in JSON{
                    let event = Event()
                    
                    event.name = eventItem["title"] as? String
                    event.category = eventItem["category"] as? String
                    event.id = eventItem["event_id"] as? String
                    event.about = eventItem["description"] as? String
                    event.date = stringFrom(utcDateString: eventItem["date"] as? String) + " " + stringFrom(utcTimeString: eventItem["time"] as? String)
                    event.isPrivate = eventItem["isprivate"] as? String == "1"
                    event.creatorID = eventItem["id"] as? String
                    event.status = eventItem["status"] as? String
                    event.tags = eventItem["tags"] as? [String]
                    
                    events.append(event)
                }
                completion(events)
            }
            else { completion(nil)}
        }
    }
    
    static func vote(eventID: String, answer: String){
        //    answer:
        //    1 – Иду
        //    2 – Возможно иду
        //    3 – не иду
        //
        
        //        Alamofire.request(URLs.vote, parameters: ["event_id": eventID, "answer": answer, "token": Profile.me.token!]) .responseJSON { response in
        //
        //            print(response.request!)
        //            print(response.result.value!)
        //
        //            if let JSON = response.result.value as? [String: AnyObject]
        //            {
        //
        //            }
        //        }
    }
    
    static func unVote(eventID: String, answer: String){
        //        //    URL: 52.41.159.42/api/geteventsbyid?user_id=&token=
        //
        //        Alamofire.request(URLs.unvote, parameters: ["event_id": eventID, "answer": answer, "token": Profile.me.token!]) .responseJSON { response in
        //
        //            print(response.result.value!)
        //
        //            if let JSON = response.result.value as? [String: AnyObject]
        //            {
        //
        //            }
        //        }
    }
    
    //    static func getFriendsCount()
    //    {
    //        Alamofire.request("http://52.41.159.42/api/getfriendcount", parameters: ["id": Profile.me.id!, "token": Profile.me.token!]) .responseJSON { response in
    //
    //            print(response.result.value!)
    //
    //            if let JSON = response.result.value as? [String: AnyObject]
    //            {
    //                //NotificationCenter.default.post(name: NNGetFriendCount, object: nil, userInfo: JSON)
    //            }
    //        }
    //    }
    //
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
}

extension Date{
    func formatDate() -> String{
        let f = DateFormatter()
        f.dateFormat = "yyyy-MM-dd"
        return f.string(from: self).replacingOccurrences(of: "UTC", with: "")
    }
}

extension NSMutableData {
    
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
