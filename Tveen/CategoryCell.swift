//
//  CategoryCell.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 11/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var selectedView: UIView!
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var im: UIImageView!
    
    override func awakeFromNib(){
        super.awakeFromNib()
        
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: frame.width/2, height: frame.height))
        gradient.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        gradient.startPoint = CGPoint(x:0 , y: 1)
        gradient.endPoint =  CGPoint(x:1 , y: 1)
        im.layer.insertSublayer(gradient, at: 0)
    }

}
