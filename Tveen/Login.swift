//
//  ViewController.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 11/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKShareKit

protocol LoginRouterDelegate {
    func showRegistrationController()
    func showMainControllerWith(model: LoginModel)
}


class LoginViewController: UIViewController {
    
    @IBOutlet weak var login: TveenTextField!
    @IBOutlet weak var password: TveenTextField!
    
    @IBOutlet weak var loginButton: TveenButton!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet var lockerView: UIView!
    
    var routerDelegate: LoginRouterDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRedPinkGradient(view: self.view)
        
        containerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(finishEditing)))
        
        login.field.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
        password.field.addTarget(self, action: #selector(valueChanged), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    

    @IBAction func testCreateEvent(_ sender: Any) {
    
        let params: [String : Any] = [
            "tags":["первый тест", "второй тест"],
            "location": ["city":"Moscow","country":"Russia","region":"Moscow", "coordinates":[55.709416, 37.766445]],
            "time": ["start":"2017/03/14 09:00:00", "end":"2017/03/16 09:00:00", "deadline":"2017/03/13 09:00:00"],
            "info": ["min_count": 3,
                     "max_count": 10,
                     "min_age": 18,
                     "max_age": 30,
                     "type": 2,
                     "title":"test name",
                     "description":"test description",
                     "public_access":true,
                     "main_image": "http://kingofwallpapers.com/picture/picture-018.jpg"
            ],
            "images": ["http://kingofwallpapers.com/picture/picture-008.jpg"]
        ]
        
                EventManager.sharedManager.createEvent(parameters: params, success: { (model) in
                    print(model)
                }) { (error) in
                    self.showAlert(title: "Ошибка", message: error.domain)
                }
        
    
    }
    
    func finishEditing() {
        view.endEditing(true)
    }
    
    func valueChanged(){
        loginButton.isHidden = login.field.text == "" && password.field.text == ""
    }
    
    //MARK: - Actions
    
    @IBAction func loginPressed(_ sender: AnyObject){
        spinner(isShow: true)
        AuthorizationManager.shared.login(login: login.text!, password: password.text!, success: { (model) in
            print(model)
            self.routerDelegate.showMainControllerWith(model: model)
            
            self.spinner(isShow: false)
        }) { (error) in
            self.showAlert(title: "Ошибка!", message: error.domain)
            
            self.spinner(isShow: false)
        }

    }
    
    @IBAction func registrationButtonAction(_ sender: Any) {
        routerDelegate.showRegistrationController()
    }
    
    func showAlert(_ title: String){
        
        spinner(isShow: false)
        
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        let deleteAction = UIAlertAction(title: "Закрыть", style: UIAlertActionStyle.default, handler: {(alert :UIAlertAction!) in
            alertController.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(deleteAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
         //   bottomOffset.constant = keyboardSize.height
            UIView.animate(withDuration: 0.2, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func keyboardWillHide(_ notification: Notification){
        if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
         //   bottomOffset.constant = 0
            UIView.animate(withDuration: 0.2, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let button = UIBarButtonItem()
        button.title = ""
        button.style = .plain
        navigationItem.backBarButtonItem = button
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back@2x.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }
    
    //MARK: - Private
    
    private func spinner(isShow:Bool) {
        if isShow == true {
            self.loginButton.isHidden = true
            self.spinner.startAnimating()
        } else {
            self.loginButton.isHidden = false
            self.spinner.stopAnimating()
        }
    }
    
}



