//
//  String+EncodeBase64.swift
//  Tveen
//
//  Created by Lyonich on 02.02.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}
