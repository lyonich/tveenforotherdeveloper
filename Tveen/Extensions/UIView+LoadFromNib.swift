//
//  UIView+LoadFromNib.swift
//  Tveen
//
//  Created by Lyonich on 24.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

extension UIView {
  class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> UIView? {
    return UINib(
      nibName: nibNamed,
      bundle: bundle
      ).instantiate(withOwner: nil, options: nil)[0] as? UIView
  }
}
