//
//  UIViewController+Alerts.swift
//  Tveen
//
//  Created by Lyonich on 30.01.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

extension UIViewController {
     func showAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        alert.view.tintColor = UIColor.red
        
        self.present(alert, animated: true, completion: nil)
    }
}
