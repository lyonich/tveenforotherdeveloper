//
//  DropdownView.swift
//  Tveen Real Estate
//
//  Created by Kirill Dobryakov on 30/01/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
//import FontAwesome_swift

//TODO: replacefontawesome

@IBDesignable class TveenTextField: UIView, UIPickerViewDataSource, UIPickerViewDelegate{
    
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageToBorderConstraint: NSLayoutConstraint!
    var pickerHeight: CGFloat = 180
    
    @IBOutlet weak var validImage: UIImageView!
    
    @IBOutlet weak var bottomSeparatorView: UIView!
    @IBOutlet weak var topSeparatorView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var icon: UIImageView!
    var view: UIView!
    
    @IBOutlet weak var butt: UIButton!
    @IBOutlet weak var field: UITextField!
    
    @IBOutlet weak var imageConstraint: NSLayoutConstraint!
    
//    var delegate: MoveContentDelegate?
    
    var currentPickerArray = [String](){
        didSet{
            
            showGeneralPicker = true
            
            generalPicker!.reloadInputViews()
            generalPicker!.picker.reloadInputViews()
            generalPicker!.picker.reloadAllComponents()
        }
    }
    
    var valid: Bool = false{
        didSet{
            validImage.isHidden = false
            
          //  validImage.image = valid ? UIImage.fontAwesomeIcon(name: .check, textColor: UIColor.white, size: CGSize(width: 20, height: 20)) : UIImage.fontAwesomeIcon(name: .times, textColor: UIColor.white, size: CGSize(width: 20, height: 20))
        }
    }
    
    var text: String?{
        set(text){
            field.text = text
        }
        get{
            return field.text
        }
    }
    
    @IBInspectable var separatorColor: UIColor = UIColor.white{
        didSet{
            topSeparatorView.backgroundColor = separatorColor
            bottomSeparatorView.backgroundColor = separatorColor
        }
    }
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenTextField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    @IBInspectable var placeholderColor: UIColor = UIColor.white{
        didSet{
            if let plac = field.attributedPlaceholder{
                field.attributedPlaceholder = NSAttributedString(string:String(describing: plac).replacingOccurrences(of: "{", with: ""), attributes:[NSForegroundColorAttributeName: placeholderColor, NSFontAttributeName: UIFont(name: "SFUIText-Regular", size: 16)!])
            }
        }
    }
    
    @IBAction func buttTouched(_ sender: AnyObject){
       // openPicker()
    }
    
    @IBInspectable var showButton: Bool = false{
        didSet{
            butt.isHidden = !showButton
        }
    }
    
    @IBInspectable var forRegistration: Bool = false{
        didSet{
            
            icon.isHidden = forRegistration
            imageConstraint.constant = forRegistration ? 20 : 47
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var forMenu: Bool = false{
        didSet{
            imageToBorderConstraint.constant = 0
            imageWidthConstraint.constant  = 23
            imageHeightConstraint.constant = 23
            imageConstraint.constant = 37
            
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var fontSize: CGFloat?{
        didSet{
            field.font = field.font?.withSize(fontSize!)
        }
    }
    
    @IBInspectable var placeholder: String?{
        get{
            return String(field.attributedPlaceholder!.string)
        }
        set(text){
            field.attributedPlaceholder = NSAttributedString(string: text!, attributes:[NSForegroundColorAttributeName: placeholderColor, NSFontAttributeName: UIFont(name: "SFUIText-Regular", size: 16)!])
        }
    }
    
    @IBInspectable var topSeparator: Bool = false{
        didSet{
            topSeparatorView.isHidden = !topSeparator
        }
    }
    
    @IBInspectable var bottomSeparator: Bool = false{
        didSet{
            bottomSeparatorView.isHidden = !bottomSeparator
        }
    }
    
    @IBInspectable var fieldText: String?{
        get { return field.text }
        set(text) { field.text = text! }
    }
    
    @IBInspectable var color: UIColor = UIColor.green{
        didSet{
            backView.backgroundColor = color
        }
    }
    
    @IBInspectable var textColor: UIColor = UIColor.green{
        didSet{
            field.textColor = textColor
        }
    }
    
    @IBInspectable var opacity: CGFloat = 255{
        didSet{
            backView.backgroundColor = backView.backgroundColor?.withAlphaComponent(opacity/255)
        }
    }
    
    @IBInspectable var iconWidth: CGFloat = 15{
        didSet{
            imageWidthConstraint.constant = iconWidth
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var iconHeight: CGFloat = 17{
        didSet{
            imageWidthConstraint.constant = iconHeight
            layoutIfNeeded()
        }
    }
    
    @IBInspectable var im: UIImage = UIImage(){
        didSet{
            icon.image = im
        }
    }
    
    @IBInspectable var secure: Bool = false{
        didSet{
            field.isSecureTextEntry = secure
        }
    }
    
    @IBInspectable var phone: Bool = false{
        didSet{
            field.keyboardType = .phonePad
        }
    }
    
    @IBInspectable var mail: Bool = false{
        didSet{
            field.keyboardType = .emailAddress
        }
    }
    
    @IBInspectable var identifier: String?
    
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor?{
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    var datePicker: TveenDatePicker?
    
    @IBInspectable var showDatePicker : Bool = false{
        didSet{
            if showDatePicker{
                showDatePicker = false
                showGeneralPicker = false
                
                datePicker = TveenDatePicker(frame: CGRect(x: 0, y: UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: pickerHeight))
                datePicker!.done.target = self
                datePicker!.done.action = #selector(donePressed)
                datePicker!.cancel.target = self
                datePicker!.cancel.action = #selector(cancelPressed)
                butt.isHidden = false
                butt.addTarget(self, action: #selector(openPicker), for: .touchUpInside)
                superview!.superview!.superview!.addSubview(datePicker!)
            }
            else{
                datePicker?.removeFromSuperview()
                datePicker = nil
            }
        }
    }
    
    var generalPicker: TveenPicker?
    
    @IBInspectable var showGeneralPicker : Bool = false{
        didSet{
            if showGeneralPicker{
                
                showDatePicker = false
                showGeneralPicker = false
                
                generalPicker = TveenPicker(frame: CGRect(x: 0, y:  UIScreen.main.bounds.height, width: UIScreen.main.bounds.width, height: pickerHeight))
                
                generalPicker!.picker.dataSource = self
                generalPicker!.picker.delegate = self
                generalPicker!.done.target = self
                generalPicker!.done.action = #selector(donePressed)
                generalPicker!.cancel.target = self
                generalPicker!.cancel.action = #selector(cancelPressed)
                butt.isHidden = false
                butt.addTarget(self, action: #selector(openPicker), for: .touchUpInside)
                superview!.superview!.superview!.addSubview(generalPicker!)
            }
            else{
                generalPicker?.removeFromSuperview()
                generalPicker = nil
            }
        }
    }
    
    func donePressed(){
        if let picker = self.datePicker{
            let formatter = DateFormatter()
            formatter.dateStyle = DateFormatter.Style.long
            
            field.text = formatter.string(from: picker.picker.date)
        }
        else if self.generalPicker != nil{
            field.text = currentPickerArray[selectedRow]
        }
        
        closePicker()
    }
    
    func cancelPressed(){
        field.text = ""
        closePicker()
    }
    
    @IBInspectable var imageColor: UIColor?{
        didSet{
            icon.image! = icon.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            icon.tintColor = imageColor
        }
    }
    
    func closePicker(){
//        delegate?.close()
        
        hidePicker()
    }
    
    func hidePicker(){
        UIView.animate(withDuration: 0.2, animations: {finished in
            
            if let picker = self.datePicker{
                picker.frame = CGRect(origin: CGPoint(x: 0, y: UIScreen.main.bounds.size.height), size: picker.frame.size)
            }
            else if let picker = self.generalPicker{
                picker.frame = CGRect(origin: CGPoint(x: 0, y: UIScreen.main.bounds.size.height), size: picker.frame.size)
            }
        })
    }
    
    func openPicker(){
//        delegate?.open(pickerHeight)
        
        UIView.animate(withDuration: 0.2, animations: {finished in
            
            if let picker = self.datePicker{
                picker.frame = CGRect(origin: CGPoint(x: 0, y: UIScreen.main.bounds.size.height - picker.frame.height), size: picker.frame.size)
            }
            else if let picker = self.generalPicker{
                picker.frame = CGRect(origin: CGPoint(x: 0, y: UIScreen.main.bounds.size.height - picker.frame.height), size: picker.frame.size)
            }
        })
    }
    
    var selectedRow = Int()
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return currentPickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currentPickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        selectedRow = row
    }
}
