//
//  TveenPicker.swift
//  Tveen Real Estate
//
//  Created by Kirill Dobryakov on 01/02/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

class TveenPicker: UIView{
    @IBOutlet weak var picker: UIPickerView!
    
    var view: UIView!
    
    @IBOutlet weak var cancel: UIBarButtonItem!
    @IBOutlet weak var done: UIBarButtonItem!
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenPicker", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
