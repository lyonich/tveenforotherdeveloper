//
//  Utilities.swift
//  Tveen
//
//  Created by Lyonich on 14.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import Foundation
import SwiftyJSON

typealias FailureWithError = (NSError) -> Void

func convertParameterToServerFormat(parameters:[String : Any]) -> [String : String] {
    let json = JSON(object: parameters)
    
    return ["data":String(utf8String: json.rawString()!.cString(using: .utf8)!)!]
}

func addRedPinkGradient(view:UIView) {
    let gradient = CAGradientLayer()
    
    gradient.frame = view.bounds
    gradient.colors = [Colors.gradientRed.cgColor, Colors.gradientPink.cgColor]
    
    view.layer.insertSublayer(gradient, at: 0)
}
