//
//  ExtendedSearchViewController.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 24/01/2017.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class ExtendedSearchViewController: UIViewController {
    @IBOutlet weak var pickerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var scopeLabel: UILabel!
    @IBOutlet weak var datePickerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var scopeField: TveenTextField!
    @IBOutlet weak var amountField: TveenTextField!
    @IBOutlet weak var ageField: TveenTextField!
    @IBOutlet weak var startField: TveenTextField!
    @IBOutlet weak var endField: TveenTextField!
    @IBOutlet weak var hideCurrentSwitch: UISwitch!
    @IBOutlet weak var joinAllowedSwitch: UISwitch!
    @IBOutlet weak var tveenPicker: TveenPicker!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    
    var currentLabel = UILabel()
    
    let age = ["0"]
    
    var minCount = 0
    var maxCount = 99999
    
    var scope = ["Мой город", "Мой регион", "Моя страна", "Весь мир"]
    
    var selectedMinAge = Int()
    var selectedMaxAge = Int()
    var selectedMinAmount = Int()
    var selectedMaxAmount = Int()
    
    
    
    let scopePickerTag = 0
    let amountPickerTag = 1
    let agePickerTag = 2
    
    
    @IBOutlet weak var datePicker: TveenDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationController()
        
        datePicker.picker.datePickerMode = .dateAndTime
        
        tveenPicker.picker.delegate = self
        tveenPicker.picker.dataSource = self
        tveenPicker.done.target = self
        tveenPicker.done.action = #selector(pickerDonePressed)
        tveenPicker.cancel.target = self
        tveenPicker.cancel.action = #selector(pickerCancelPressed)
        
        datePicker.done.target = self
        datePicker.done.action = #selector(dateDonePressed)
        datePicker.cancel.target = self
        datePicker.cancel.action = #selector(dateCancelPressed)
        
        ageField.butt.addTarget(self, action: #selector(agePressed), for: .touchUpInside)
        
        startField.butt.addTarget(self, action: #selector(beginningPressed), for: .touchUpInside)
        endField.butt.addTarget(self, action: #selector(endPressed), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        datePicker.picker.addTarget(self, action: #selector(dateUpdate), for: UIControlEvents.valueChanged)
        amountField.butt.addTarget(self, action: #selector(amountPressed), for: .touchUpInside)
        
        scopeField.butt.addTarget(self, action: #selector(scopePressed), for: .touchUpInside)
    }
    
    func setupNavigationController(){
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Готово", style: .done, target: self, action: #selector(searchDone))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отмена", style: .done, target: self, action: #selector(searchCancelled))
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 480, height: 44))
        label.numberOfLines = 2
        label.text =  "Расширенный поиск"
        label.textAlignment = .center
        label.textColor = .white
        label.font = UIFont(name: "SFUIText-Regular", size: 15)
        
        navigationItem.titleView = label
    }
    

    
    func dateUpdate(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy    HH:mm"
        
        let dateString = formatter.string(from: datePicker.picker.date)
        currentLabel.text = dateString
    }
    
    
    func beginningPressed(){
        if let eDate = dateFromString(endLabel.text!){
            datePicker.picker.maximumDate = eDate
        }
        else{
            datePicker.picker.maximumDate = nil
        }
        
        datePicker.picker.minimumDate = Date()
        
        datePicker.picker.reloadInputViews()
        
        currentLabel = startLabel
        openDatePicker(currentLabel)
    }
    
    func endPressed(){
        
        if let eDate = dateFromString(startLabel.text!){
            datePicker.picker.minimumDate = eDate
        }
        else{
            datePicker.picker.minimumDate = Date()
        }
        
        datePicker.picker.maximumDate = nil
        
        datePicker.picker.reloadInputViews()
        
        currentLabel = endLabel
        openDatePicker(currentLabel)
    }
    
    func searchDone(){
        dismiss(animated: true)
    }
    
    func searchCancelled(){
        dismiss(animated: true)
    }
    
    
    func scopePressed(){
        tveenPicker.picker.tag = scopePickerTag
        currentLabel = scopeLabel
        openPicker()
    }
    
    func agePressed(){
        tveenPicker.picker.tag = agePickerTag
        currentLabel = ageLabel
        openPicker()
    }
    
    
    func amountPressed(){
        tveenPicker.picker.tag = amountPickerTag
        currentLabel = amountLabel
        openPicker()
    }
    
    func openDatePicker(_ label: UILabel){
        pickerDonePressed()
        
        if datePickerBottomConstraint.constant == 0{
            UIView.animate(withDuration: 0.1, animations: { x in
                self.datePickerBottomConstraint.constant = self.datePickerBottomConstraint.constant - 20
                self.view.layoutIfNeeded()
            }, completion: {x in
                
                UIView.animate(withDuration: 0.1, animations: { x in
                    self.datePickerBottomConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
            })
        }
        
        if label.text != ""{
            datePicker.picker.date = dateFromString(label.text!)!
        }
        
        bottom.constant = datePicker.frame.height
        
        datePickerBottomConstraint.constant = 0
        layout()
    }
    
    func dateFromString(_ string: String) -> Date?{
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy  HH:mm"
        
        return formatter.date(from: string)
    }
    
    func openPicker(){
       
        tveenPicker.picker.reloadAllComponents()
        dateDonePressed()
        
//        if currentLabel.text != "" && tveenPicker.picker.tag == scopePickerTag{
//            tveenPicker.picker.selectRow(Int(currentLabel.text!)!, inComponent: 0, animated: true)
//        }
        
        if pickerBottomConstraint.constant == 0{
            UIView.animate(withDuration: 0.1, animations: { x in
                self.pickerBottomConstraint.constant = self.pickerBottomConstraint.constant - 20
                self.view.layoutIfNeeded()
            }, completion: {x in
                
                UIView.animate(withDuration: 0.1, animations: { x in
                    self.pickerBottomConstraint.constant = 0
                    self.view.layoutIfNeeded()
                })
            })
        }
        
        bottom.constant = tveenPicker.frame.height
        pickerBottomConstraint.constant = 0
        
        layout()
    }
    
    func layout(){
        UIView.animate(withDuration: 0.2, animations: {self.view.layoutIfNeeded()})
    }
    
    func dateDonePressed(){
        bottom.constant = 0
        datePickerBottomConstraint.constant = -datePicker.frame.height
        layout()
    }
    
    func dateCancelPressed(){
        bottom.constant = 0
        currentLabel.text = ""
        datePickerBottomConstraint.constant = -datePicker.frame.height
        layout()
    }
    
    func close(){
        view.endEditing(true)
        pickerDonePressed()
        dateDonePressed()
    }
    
    
    func pickerDonePressed(){
        bottom.constant = 0
        pickerBottomConstraint.constant = -tveenPicker.frame.height
        layout()
    }
    
    func pickerCancelPressed(){
        bottom.constant = 0
        currentLabel.text = ""
        pickerBottomConstraint.constant = -tveenPicker.frame.height
        layout()
    }
    
    func keyboardWillShow(_ notification: Notification){
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            bottom.constant = keyboardSize.height
            view.layoutIfNeeded()
        }
    }
    
    func keyboardWillHide(_ notification: Notification){
        bottom.constant = 0
        view.layoutIfNeeded()
    }
    
}

extension ExtendedSearchViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return   pickerView.tag == agePickerTag || pickerView.tag == amountPickerTag ? 2 : 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      
        switch pickerView.tag {
        case scopePickerTag: return scope.count
        case agePickerTag:
            return  component == 0 ? (selectedMaxAge == 0 ? maxCount : selectedMaxAge + 1): maxCount - selectedMinAge
        case amountPickerTag:
            return  component == 0 ? (selectedMaxAmount == 0 ? maxCount : selectedMaxAmount + 1): maxCount - selectedMinAmount
        default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case scopePickerTag: return scope[row]
        case agePickerTag:
            return  String(component == 0 ? row: row + selectedMinAge)
        case amountPickerTag:
            return  String(component == 0 ? row: row + selectedMinAmount)
        default: return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    
        
        switch pickerView.tag {
        case scopePickerTag:  currentLabel.text = scope[row]
        case agePickerTag:
            component == 0 ? (selectedMinAge = row) : (selectedMaxAge = row + selectedMinAge)
            if selectedMaxAge < selectedMinAge{
                selectedMaxAge = selectedMinAge
            }
            
            if component == 0 && selectedMaxAge != 0{
                pickerView.selectRow(selectedMaxAge - selectedMinAge, inComponent: 1, animated: false)
            }
            
            currentLabel.text = "от \(String(selectedMinAge))    до \(String(selectedMaxAge))"
        case amountPickerTag:
              component == 0 ? (selectedMinAmount = row) : (selectedMaxAmount = row + selectedMinAmount)
              if selectedMaxAmount < selectedMinAmount{
                selectedMaxAmount = selectedMinAmount
              }
              
              if component == 0 && selectedMaxAmount != 0{
                pickerView.selectRow(selectedMaxAmount - selectedMinAmount, inComponent: 1, animated: false)
              }
            
              
              currentLabel.text = "от \(String(selectedMinAmount))    до \(String(selectedMaxAmount))"
        default: break
        }
        
        
        tveenPicker.picker.reloadAllComponents()
    }
}

