//
//  TveenView.swift
//  Wilk Real Estate
//
//  Created by Kirill Dobryakov on 31/01/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

@IBDesignable class TveenView: UIView{
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0{
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor?{
        didSet{
            layer.borderColor = borderColor?.cgColor
        }
    }
}
