//
//  TveenDatePicker.swift
//
//
//  Created by Kirill Dobryakov on 30/05/16.
//
//

import UIKit

class TveenDatePicker: UIView{
    @IBOutlet weak var done: UIBarButtonItem!
    @IBOutlet weak var picker: UIDatePicker!
    
    @IBOutlet weak var cancel: UIBarButtonItem!
    
    var view: UIView!
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenDatePicker", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
}
