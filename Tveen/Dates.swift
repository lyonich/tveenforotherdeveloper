//
//  Dates.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 06/02/2017.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

struct Dates {
    static let serverDateFormat = "YYYY/MM/dd hh:mm:ss"
    static let userDateFormat = "dd MMMM YYYY hh:mm"
    static let feedDateFormat = "dd MMMM hh:mm"
}

func convertDateFormater(date: String, finalDateFormate: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = Dates.serverDateFormat
    
    guard let date = dateFormatter.date(from: date) else {
        return ""
    }
    
    dateFormatter.dateFormat = finalDateFormate
    
    let timeStamp = dateFormatter.string(from: date)
    
    return timeStamp
}
