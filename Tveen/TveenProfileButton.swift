//
//  TveenProfileButton.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 16/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

@IBDesignable class TveenProfileButton: UIView{
    
    @IBOutlet weak var butt: UIButton!
    @IBOutlet weak var leftBorder: UIView!
    @IBOutlet weak var rightBorder: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var bottomLabel: UILabel!
    
    var view: UIView!
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenProfileButton", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    @IBInspectable var name: String = "Name"{
        didSet{
            topLabel.text = name
        }
    }
    
    @IBInspectable var value: String = "Value"{
        didSet{
            bottomLabel.text = value
        }
    }
    
    @IBInspectable var rightSeparator: Bool = false{
        didSet{
            rightBorder.isHidden = !rightSeparator
        }
    }
    
    @IBInspectable var leftSeparator: Bool = false{
        didSet{
            leftBorder.isHidden = !leftSeparator
        }
    }
}
