//
//  TipsCell.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 25/01/2017.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

class TipsCell: UITableViewCell {

    @IBOutlet weak var tipImageView: UIImageView!
    @IBOutlet weak var tipLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
