//
//  TveenAvatarImage.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 16/07/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import QuartzCore

@IBDesignable class TveenAvatarImage: UIImageView{
    @IBOutlet weak var greyImage: UIImageView!
    
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var im: UIImageView!
    var view: UIView!
    
    override init(frame: CGRect){
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    func xibSetup(){
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        view.backgroundColor = UIColor.clear
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TveenAvatarImage", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    //TODO: change placeholder image
    @IBInspectable var img: UIImage = UIImage() {
        didSet{
            im.image = img
            im.layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var hideCameraIcon: Bool = true {
        didSet{
            cameraImage.isHidden = hideCameraIcon
            greyImage.isHidden = hideCameraIcon
        }
    }
    
    @IBInspectable var shadowOffsetX: CGFloat = CGFloat(){
        didSet{
            view.layer.masksToBounds = false
            view.layer.shadowOffset = CGSize(width: shadowOffsetX, height: view.layer.shadowOffset.height)
        }
    }
    @IBInspectable var shadowOffsetY: CGFloat = CGFloat(){
        didSet{
            view.layer.masksToBounds = false
            view.layer.shadowOffset = CGSize(width: view.layer.shadowOffset.width, height: shadowOffsetY)
        }
    }
    
    //Shadow code
    
    @IBInspectable var shadowRadius: CGFloat = 0{
        didSet{
            view.layer.masksToBounds = false
            view.layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 0{
        didSet{
            view.layer.masksToBounds = false
            view.layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            im.layer.cornerRadius = cornerRadius
            im.layer.masksToBounds = cornerRadius > 0
        }
    }
}
