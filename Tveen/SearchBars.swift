//
//  SearchBars.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 21/03/2017.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

func setTitleForSearchBarCancelButton(searchBar: UISearchBar) {
    searchBar.setValue("Отмена", forKey:"_cancelButtonText")
}


