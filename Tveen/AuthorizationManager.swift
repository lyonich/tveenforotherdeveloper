//
//  AuthorizationManager.swift
//  Tveen
//
//  Created by Lyonich on 14.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias SuccessWithRegistrationData = (LoginModel) -> Void

//TODO: - Возможно необходимо будет вынести модель
class LoginModel {
    var token:      String  = ""
    var login:      String  = ""
    var password:   String  = ""
    var phone:      String  = ""
    var email:      String  = ""
    var uuid:       String  = ""
    var birthday:   String  = ""
    var name:       String  = ""
    var description: String = ""
    var country:    String  = ""
    var sex:        Int     = 0
    var help:       Bool    = false
    var avatarURL: String   = ""
    var category = [Int]()
    var friendsIDs = [String]()
    var followersIDs = [String]()
    var followingsIDs = [String]()
}

class AuthorizationManager: NSObject {
    
    static let shared = AuthorizationManager()
    
    func registration(login:String, password: String, phone: String, birthday:String, success: @escaping SuccessWithRegistrationData, failure: @escaping FailureWithError) {
        let notJSONparameters = ["login":login, "pass_hash":password.md5(), "birthday":birthday, "phone": phone ] as [String : String]
        let JSONparameters = convertParameterToServerFormat(parameters: notJSONparameters)
        
        Alamofire.request(APIUrls.registration, method: .put, parameters: JSONparameters, encoding: URLEncoding.httpBody).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
            
                let registrationModel = self.parseLoginResponse(json: json)
                success(registrationModel)

            case .failure(_):
                if response.response?.statusCode == 409 {
                    let error = NSError(domain: "Такой логин уже существует в системе", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else {
                    let error = NSError(domain: "Неизвестная ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                }
            }
        }
    }
    
    func login(login: String, password: String, success: @escaping SuccessWithRegistrationData, failure: @escaping FailureWithError) {
        let notJSONparameters = ["login":login, "pass_hash":password.md5()] as [String : String]
        let JSONparameters = convertParameterToServerFormat(parameters: notJSONparameters)
        
        Alamofire.request(APIUrls.login, method: .post, parameters: JSONparameters, encoding: URLEncoding.httpBody).responseJSON { (response) in
            switch response.result {
            case .success(let value): let json = JSON(value)
                print(json)
                
                if response.response?.statusCode == 204 {
                    let error = NSError(domain: "Такого пользователя не существует", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else if response.response?.statusCode == 200 {
                    print(json)
                    
                    let loginModel = self.parseLoginResponse(json: json)
                    success(loginModel)
                }
            case .failure(_):
                if response.response?.statusCode == 400 {
                    let error = NSError(domain: "Неправильный пароль", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                } else {
                    let error = NSError(domain: "Неизвестная ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                    failure(error)
                }
            }
        }
    }
    
    //MARK: - Private
    
    func parseLoginResponse(json: JSON) -> LoginModel {
        let data = LoginModel()
        
        if let token = json["token"].string {
            data.token = token
        }
        
        if let login = json["base"]["login"].string {
            data.login = login
        }
        
        if let password = json["base"]["pass_hash"].string {
            data.password = password
        }
        
        if let avatarURL = json["base"]["avatar_url"].string {
            data.avatarURL = avatarURL
        }
        
        if let phone = json["contact"]["phone"].string {
            data.phone = phone
        }
        
        if let email = json["contact"]["email"].string {
            data.email = email
        }
        
        if let uuid = json["base"]["uuid"].string {
            data.uuid = uuid
        }
        
        if let birthday = json["base"]["birthday"].string {
            data.birthday = birthday
        }
        
        if let token = json["token"].string {
            if token != "" {
                TokenManager.sharedInstance.saveToken(token)
            }
        }
        
        if let name = json["base"]["name"].string {
            data.name = name
        }
        
        if let description = json["base"]["description"].string {
            data.description = description
        }
        
        if let country = json["geo"]["country"].string {
            data.country = country
        }
        
        if let sex = json["base"]["sex"].int {
            data.sex = sex
        }
        
        if let help = json["base"]["help"].bool {
            data.help = help
        }
        
        if let category = json["likes"]["category"].arrayObject {
            data.category = category as! [Int]
        }
        
        if let friends = json["connecting"]["friends"].arrayObject as? [String]{
            data.friendsIDs = friends
        }
        
        if let followers = json["connecting"]["followers"].arrayObject as? [String]{
            data.followersIDs = followers
        }
        
        if let followings = json["connecting"]["followings"].arrayObject as? [String]{
            data.followingsIDs = followings
        }
        
        
        

        return data
    }
    
}
