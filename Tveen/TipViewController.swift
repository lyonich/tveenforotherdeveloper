//
//  TipViewController.swift
//  Tveen
//
//  Created by Lyonich on 13.02.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import UIKit

//TODO: Сделать подсказки для табов!

enum TypeTip:Int {
    case categories
    case menu
    case profile
    case deadlineOne
    case deadlineTwo
    case publicTip
    case rate
    case media
    case friendEvent
    case feed
    case welcomeSearch
    case searchButton
    case categorySearch
    case tagsSearch
    case sorted
    case calendar
    case mapRedPin
    case stayPin
    case tab1
    case tab2
    case tab3
    case tab4
}

class TipViewController: UIViewController {
    
    @IBOutlet weak var buttonImageView: UIImageView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    var typeTip: TypeTip = .menu
    var i: Int = 0
    
    //MARK: - Init
    
    init(typeTip: TypeTip ) {
        super.init(nibName: "TipViewController", bundle: nil)
        
        self.typeTip = typeTip
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        
        alertView.layer.cornerRadius = 10
        
        switch typeTip {
        case .categories:
            configurateCategoriesTip()
        case .menu:
            configurateMenuTip()
        case .profile:
            configurateProfileTip()
        case .deadlineOne:
            configurateDeadlineStepOne()
        case .deadlineTwo:
            configurateDeadlineStepTwo()
        case .publicTip:
            configuratePublicTip()
        case .rate:
            configurateRateTip()
        case .media:
            configurateMediaTip()
        case .friendEvent:
            configurateFriendEventTip()
        case .feed:
            configurateFeedTip()
        case .welcomeSearch:
            configurateWelcomeSearchTip()
        case .searchButton:
            configurateWelcomeSearchTip()
        case .categorySearch:
            configurateCategorySearchTip()
        case .tagsSearch:
            configurateTagSearchTip()
        case .sorted:
            configurateSortedTip()
        case .calendar:
            configurateCalendarTip()
        case .mapRedPin:
            configurateMapRedPinTip()
        case .stayPin:
            stayMapPin()
        case .tab1:
            configurateTab1()
        case .tab2:
            configurateTab2()
        case .tab3:
            configurateTab3()
        case .tab4:
            configurateTab4()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Actions
    
    @IBAction func hideButtonAction(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func changeTipButtonAction(_ sender: Any) {
        
        buttonImageView.layer.shadowOpacity = 0.0

        switch i {
        case TypeTip.categories.rawValue:
            configurateCategoriesTip()
        case TypeTip.menu.rawValue:
            configurateMenuTip()
        case TypeTip.profile.rawValue:
            configurateProfileTip()
        case TypeTip.deadlineOne.rawValue:
            configurateDeadlineStepOne()
        case TypeTip.deadlineTwo.rawValue:
            configurateDeadlineStepTwo()
        case TypeTip.publicTip.rawValue:
            configuratePublicTip()
        case TypeTip.rate.rawValue:
            configurateRateTip()
        case TypeTip.media.rawValue:
            configurateMediaTip()
        case TypeTip.friendEvent.rawValue:
            configurateFriendEventTip()
        case TypeTip.feed.rawValue:
            configurateFeedTip()
        case TypeTip.welcomeSearch.rawValue:
            configurateWelcomeSearchTip()
        case TypeTip.searchButton.rawValue:
            configurateWelcomeSearchTip()
        case TypeTip.categorySearch.rawValue:
            configurateCategorySearchTip()
        case TypeTip.tagsSearch.rawValue:
            configurateTagSearchTip()
        case TypeTip.sorted.rawValue:
            configurateSortedTip()
        case TypeTip.calendar.rawValue:
            configurateCalendarTip()
        case TypeTip.mapRedPin.rawValue:
            configurateMapRedPinTip()
        case TypeTip.stayPin.rawValue:
            stayMapPin()
        case TypeTip.tab1.rawValue:
            configurateTab1()
        case TypeTip.tab2.rawValue:
            configurateTab2()
        case TypeTip.tab3.rawValue:
            configurateTab3()
        case TypeTip.tab4.rawValue:
            configurateTab4()
        default:
            break
        }
        
        i = i + 1
    }
    
    //MARK: - Private
    
    func configurateMenuTip() {
        
        buttonImageView.layer.shadowColor = Colors.violet.cgColor
        buttonImageView.layer.shadowOpacity = 0.8
        buttonImageView.layer.shadowRadius = 10
        buttonImageView.layer.shadowOffset = CGSize(width: 0, height: 1)
        
        titleLabel.text         = "Создание мероприятия"
        textLabel.text          = "Создавайте и делитесь своими мероприятиями с помощью этой кнопки!"
        buttonImageView.image   = #imageLiteral(resourceName: "addEventButton")
    }
    
    func configurateProfileTip() {
        titleLabel.text         = "Ваш профиль"
        textLabel.text          = "Заполните информацию о себе, чтобы о Вас узнал весь мир!"
        buttonImageView.image   = #imageLiteral(resourceName: "profileTip")
    }
    
    func configurateDeadlineStepOne() {
        titleLabel.text         = "Дедлайн"
        textLabel.text          = "Добро пожаловать на самую важную страницу создания Вашего первого мероприятия! Ознакомьтесь с некоторыми важными для корректной работы с приложением понятиями."
        buttonImageView.image   = #imageLiteral(resourceName: "deadlineTip")
    }
    
    func configurateDeadlineStepTwo() {
        titleLabel.text = "Дедлайн"
        textLabel.text = "Дедлайн мероприятия-крайний срок, к которому должно быть собрано минимальное число участников, без которых невозможно его проведение. Если до дедлайна не было собрано такое число, то мероприятие не состоится. Если для организатора неважно минимальное число человек для осуществления своего проекта, то он может не заполнять такой параметр, как минимальное число участников."
        buttonImageView.image = #imageLiteral(resourceName: "deadlineTip")
    }
    
    func configuratePublicTip() {
        titleLabel.text         = "Приватность"
        textLabel.text          = "В нашем приложении мероприятия делятся на приватные и публичные. Публичные мероприятия доступны всем, без исключения, лицам нашей социальной сети. На приватные мероприятия Вы сможете пригласить только своих друзей и подписчиков, кроме того, постами о посещениях приватных мероприятий нельзя делиться в других социальных сетях, также за них вы не сможете получить рейтинг."
        buttonImageView.image   = #imageLiteral(resourceName: "lockTip")
    }
    
    func configurateMediaTip() {
        titleLabel.text         = "Медиа"
        textLabel.text          = "Включая доступ к публикации медиа, Вы разрешаете участникам мероприятия выкладывать свои фотографии и видео в раздел «Медиа» Вашего мероприятия. Этот параметр можно будет изменить в настройках мероприятия."
        buttonImageView.image   = #imageLiteral(resourceName: "mediaTip")
    }
    
    func configurateRateTip() {
        titleLabel.text         = "Рейтинг"
        textLabel.text          = "Здесь Вы имеете уникальную возможность видеть средний рейтинг мероприятий данного пользователя, 3 его любимые категории, а также немного статистики о нем и его мероприятиях!"
        buttonImageView.image   = #imageLiteral(resourceName: "rateTip")
    }
    
    func configurateCategoriesTip() {
        titleLabel.text         = "Категории"
        textLabel.text          = "Выбранные Вами категории будут влиять на подборки мероприятий, которые мы будем составлять для Вас каждую неделю"
        buttonImageView.image   = #imageLiteral(resourceName: "categoryTip")
    }
    
    func configurateFriendEventTip() {
        titleLabel.text         = "Событие друзей"
        textLabel.text          = "«Это мероприятие, которое организовал один из наших пользователей. На этой странице Вы можете посмотреть подробную информацию о нем, узнать время начала и конца, а так же присоединиться к нему нажав на кнопку «Пойду».» «Выберите один из вариантов, чтобы присоединиться к нему. P.s. При выборе варианта «Возможно пойду »»"
        buttonImageView.image   = #imageLiteral(resourceName: "eventTip")
    }
    
    func configurateFeedTip() {
        titleLabel.text         = "Лента"
        textLabel.text          = "Здесь отображаются мероприятия, организованные Вашими друзьями и теми людьми, на которых вы подписаны"
        buttonImageView.image   = #imageLiteral(resourceName: "eventTip")
    }
    
    func configurateFirstLookEvent() {
        titleLabel.text         = "Текущие мероприятия"
        textLabel.text          = "Этим значком будут отмечаться мероприятия на которые вы идете"
        buttonImageView.image   = #imageLiteral(resourceName: "currentEventTip")
    }
    
    func configurateCategorySearchTip() {
        titleLabel.text         = "Категории поиска"
        textLabel.text          = "Выберите категорию поиска. Если вы не выберите ни одну из категорий, то поиск автоматически будет осуществляться сразу по всем."
        buttonImageView.image   = #imageLiteral(resourceName: "searchTip")
    }
    
    func configurateWelcomeSearchTip() {
        titleLabel.text         = "Категории поиска"
        textLabel.text          = "Выберите категорию поиска. Если вы не выберите ни одну из категорий, то поиск автоматически будет осуществляться сразу по всем."
        buttonImageView.image   = #imageLiteral(resourceName: "searchTip")
    }
    
    func configurateButtonSearchTip() {
        titleLabel.text         = "Категории поиска"
        textLabel.text          = "Выберите категорию поиска. Если вы не выберите ни одну из категорий, то поиск автоматически будет осуществляться сразу по всем."
        buttonImageView.image   = #imageLiteral(resourceName: "searchTip")
    }
    
    func configurateTagSearchTip() {
        titleLabel.text         = "Поиск по местам и тегам"
        textLabel.text          = "Введите название мероприятия, место проведения или тег. Названия и теги обозначаются красным цветом, места- фиолетовым."
        buttonImageView.image   = #imageLiteral(resourceName: "searchTip")
    }
    
    func configurateSortedTip() {
        titleLabel.text         = "Сортировки"
        textLabel.text          = "Отсортируйте полученные результаты запроса так, как нужно именно Вам."
        buttonImageView.image   = #imageLiteral(resourceName: "sortedTip")
    }
    
    func configurateCalendarTip() {
        titleLabel.text         = "Календарь"
        textLabel.text          = "Просмотрите Ваши запланированные события на календаре."
        buttonImageView.image   = #imageLiteral(resourceName: "calendarTip")
    }
    
    func configurateMapRedPinTip() {
        titleLabel.text         = "Карта"
        textLabel.text          = "Красными фишками на карте обозначены мероприятия, которые уже начались и продолжаются в данный момент(текущие), а фиолетовыми- те, которые запланированы."
        buttonImageView.image   = #imageLiteral(resourceName: "redPinTip")
    }
    
    func stayMapPin() {
        titleLabel.text         = "Карта"
        textLabel.text          = "Красными фишками на карте обозначены мероприятия, которые уже начались и продолжаются в данный момент(текущие), а фиолетовыми- те, которые запланированы."
        buttonImageView.image   = #imageLiteral(resourceName: "redPinTip")
    }
    
    func configurateTab1() {
        titleLabel.text         = "Мероприятия"
        textLabel.text          = "Здесь находятся Мероприятия, организованные Вами в виде сетки"
        buttonImageView.image   = #imageLiteral(resourceName: "tab1Tip")
    }
    
    func configurateTab2() {
        titleLabel.text         = "Мероприятия"
        textLabel.text          = "Мероприятия, организованные Вами в виде списка"
        buttonImageView.image   = #imageLiteral(resourceName: "tab2Tip")
    }
    
    func configurateTab3() {
        titleLabel.text         = "Мероприятия"
        textLabel.text          = "Мероприятия, организованные и посещенные Вами на карте"
        buttonImageView.image   = #imageLiteral(resourceName: "tab3Tip")
    }
    
    func configurateTab4() {
        titleLabel.text         = "Мероприятия"
        textLabel.text          = "Мероприятия с Вами в виде сетки"
        buttonImageView.image   = #imageLiteral(resourceName: "tab4Tip")
    }
    

}
