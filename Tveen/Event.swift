//
//  Event.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 21/11/2016.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
//import RealmSwift

class Event {
   
    var id = String()
    
    var owner_uuid = String()
    
    var ownerName = String()
    var ownerLogin = String()
    var ownerAvatarLink = String()
    
    var title = String()
    var description = String()
    var category =  Category()
    var type = Int()
    
    var tags : [String] = [String]()
    
    var country = String()
    var region = String()
    var city = String()
    
    var coordinates = CGPoint()
    
    var mainImageURL = String()
    var imageURLS = [String]()
    
    var minAge = Int()
    var maxAge = Int()
    
    var minCount = Int()
    var maxCount = Int()
    
    var timeStart = String()
    var timeEnd = String()
    var timeDeadline = String()
    
    var members = [String]()
    
    var shouldPostVK = false
    var shouldPostFB = false
    var shouldPostTW = false
    
    
    var isPrivate = true
    
    var isPublicAccess = false
}
