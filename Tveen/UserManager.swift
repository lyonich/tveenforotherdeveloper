//
//  UserManager.swift
//  Tveen
//
//  Created by Lyonich on 17.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

enum UserTypeEditCategory: Int {
    case base       = 1
    case likes      = 3
    case geo        = 4
    case contact    = 5
}

enum TypeAvailability: Int {
    case publicProfile  = 1
    case privateProfile = 2
}

enum TypeLocalization: String {
    case ru = "ru"
    case en = "en"
}

class UserModel {
    var name: String = ""
    var description: String = ""
    var email: String = ""
    var country: String = ""
    var sex: Int = 0
    var avatarURL: String = ""
    var category = [Int]()
    //TODO: Решить вопрос со страной / город
}

enum TypeUserSearch: Int {
    case identifier             = 1
    case category               = 2
    case tags                   = 3
    case coordinates            = 4
    case radius                 = 5
    case name                   = 6
    case date                   = 7
    case identifierOwnerEvent   = 8
    case status                 = 9
    case identifierS            = 10
    case nameUser               = 11
    case old                    = 12
    case city                   = 13
    case friends                = 14
    case subscriptions          = 15
    case subscribers            = 16
}

enum SortTypeEvent: Int {
    case quality = 1
    case distantion = 2
    case countsVistors = 3
    case duration = 4
    case startTime = 5
    case deadline = 6
    case createdDate = 7
}

class UserManager: NSObject {
    
    typealias SuccessWithUserData = (UserModel) -> Void
    typealias SuccessWithEvents = ([Event]) -> Void
    
    static let sharedManager = UserManager()
    
    var userTypeData: UserTypeEditCategory = .base

    func editUser(UUID: String,type:UserTypeEditCategory, parameters: [String : Any], success: @escaping SuccessWithUserData, failure: @escaping FailureWithError) {
        let url = APIUrls.user + UUID + "/"
        
        var typeData = [String : Any]()
        
        switch type {
        case .base:
            typeData = ["base" : parameters]
            userTypeData = .base
        case .contact:
            typeData = ["contact" : parameters]
            userTypeData = .contact
        case .geo:
            typeData = ["geo" : parameters]
            userTypeData = .geo
        case .likes:
            typeData = ["likes" : parameters]
            userTypeData = .likes
        }
        
        let prepareParameters = ["type":type.rawValue, "user":typeData] as [String : Any]
        
        let finalParameters = convertParameterToServerFormat(parameters: prepareParameters)
        
        Alamofire.request(url, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value):let json = JSON(value)
                let model = self.parseUserResponse(json: json)
                success(model)
            case .failure(_):
                var error = NSError()
                
                if response.response?.statusCode == 409 {
                    error = NSError(domain: "Неправильный тип раздела", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 401 {
                    error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 404 {
                    error = NSError(domain: "Внутренняя ошибка сервера", code: (response.response?.statusCode)!, userInfo: nil)
                } else {
                    error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                }
                
                failure(error)
            }
        }
    }
    
    func followUser(UUID: String, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let url = APIUrls.follow + UUID + "/"
        Alamofire.request(url, method: .put, parameters: nil, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            
            if response.response?.statusCode == 200 {
                success("Пользователь успешно добавлен в друзья")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }
        }
    }
    
    func unfollowUser(UUID: String, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let url = APIUrls.unfollow + UUID + "/"

        Alamofire.request(url, method: .put, parameters: nil, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            if response.response?.statusCode == 200 {
                success("Пользователь успешно удален из друзей")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }
        }
    }
    
    func addBlacklist(UUID: String, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let url = APIUrls.blacklist + UUID + "/"
        
        Alamofire.request(url, method: .put, parameters: nil, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            if response.response?.statusCode == 200 {
                success("Пользователь успешно добавлен в черный список")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                 let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                 failure(error)
            }
        }
    }
    
    func changeAvailability(type:TypeAvailability, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let prepareParameters = ["type":2] as [String : Int]
        
        let finalParameters = convertParameterToServerFormat(parameters: prepareParameters)
        
        Alamofire.request(APIUrls.availability, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            if response.response?.statusCode == 200 {
                success("Тип профиля успешно изменен")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }
        }
    }
    
    func changeTypeLocalization(type:TypeLocalization, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let prepareParameters = ["localization":"en"] as [String : String]
        
        let finalParameters = convertParameterToServerFormat(parameters: prepareParameters)
        
        Alamofire.request(APIUrls.localization, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            
            if response.response?.statusCode == 200 {
                success("Локализация профиля успешно имзенена")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }
        }
    }
    
    func changeHelpMode(mode:Bool, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let prepareParameters = ["help":mode] as [String : Bool]
        
        let finalParameters = convertParameterToServerFormat(parameters: prepareParameters)
        
        Alamofire.request(APIUrls.help, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(_):
                success("Состояние подсказок изменено")
            case .failure(_):
                var error = NSError()
                
                if response.response?.statusCode == 500 {
                    error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 401 {
                    error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 404 {
                    error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                } else {
                    error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                }
                
                failure(error)
            }
        }
    }
    
    func changePassword(password:String, success: @escaping (String?) -> Void, failure: @escaping FailureWithError) {
        let prepareParameters = ["pass_hash":password.md5()] as [String : String]
        
        let finalParameters = convertParameterToServerFormat(parameters: prepareParameters)
        
        Alamofire.request(APIUrls.password, method: .put, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(_):
                success("Пароль изменен на новый")
            case .failure(_):
                var error = NSError()
                
                if response.response?.statusCode == 500 {
                    error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 401 {
                    error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 404 {
                    error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                } else {
                    error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                }
                
                failure(error)
            }
        }
    }
    
    func clear(success:@escaping (String?) -> Void, failure: @escaping FailureWithError) {
        Alamofire.request(APIUrls.clear, method: .post, parameters: nil, encoding: URLEncoding.httpBody, headers:  TokenManager.sharedInstance.token()).responseJSON { (response) in
            if response.response?.statusCode == 200 {
                success("Данные пользователя о мероприятия очищены")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }

        }
    }
    
    func deleteAccount(success:@escaping (String?) -> Void, failure: @escaping FailureWithError) {
        Alamofire.request(APIUrls.delete, method: .delete, parameters: nil, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            if response.response?.statusCode == 200 {
                success("Пользователь удален")
            } else if response.response?.statusCode == 500 {
                let error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 401 {
                let error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else if response.response?.statusCode == 404 {
                let error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            } else {
                let error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                failure(error)
            }
        }
    }
    
    
    
    func friendSearch(type: TypeUserSearch, uuid: String, success:@escaping ([LoginModel]) -> Void,failure: @escaping FailureWithError) {
        let parameters = ["type":type.rawValue, "uuid": uuid] as [String : Any]
    
        usersSearch(parameters: parameters, success: { users in
            success(users)
        }) { error in
            failure(error)
        }
    }
    
    

    func getUserByID(uuid: String, success:@escaping (LoginModel) -> Void,failure: @escaping FailureWithError){
        
        let parameters = ["type": TypeUserSearch.identifier, "uuid": uuid] as [String : Any]
        
        userSearch(parameters: parameters, success: { users in
            success(users)
        }) { error in
            failure(error)
        }
    }
    
    
    func searchPeopleByName(name: String,  success:@escaping ([LoginModel]) -> Void,failure: @escaping FailureWithError){
        
        let parameters = ["type":11, "base": ["name" : name]] as [String : Any]
        
        usersSearch(parameters: parameters, success: { users in
            success(users)
        }) { error in
            failure(error)
        }
    }
    
    private func usersSearch(parameters: [String: Any], success:@escaping ([LoginModel]) -> Void,failure: @escaping FailureWithError){
    
        let finalParameters = convertParameterToServerFormat(parameters: parameters)
        
        print("request with parameters: \(parameters)")
        
        Alamofire.request(APIUrls.userSearch, method: .post, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value):let json = JSON(value)
            
            print(json)
            
            var people = [LoginModel]()
            
            for person in json{
                people.append(AuthorizationManager.shared.parseLoginResponse(json: person.1))
            }
            
            success(people)
                
            case .failure(_):
                var error = NSError()
                
                if response.response?.statusCode == 500 {
                    error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 401 {
                    error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 404 {
                    error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                } else {
                    error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                }
                
                failure(error)
            }
        }
    }
    
    
    private func userSearch(parameters: [String: Any], success:@escaping (LoginModel) -> Void,failure: @escaping FailureWithError){
        
        let finalParameters = convertParameterToServerFormat(parameters: parameters)
        
        print("request with parameters: \(parameters)")
        
        Alamofire.request(APIUrls.userSearch, method: .post, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value):let json = JSON(value)
            
            print(json)
            success(AuthorizationManager.shared.parseLoginResponse(json: json))
                
            case .failure(_):
                var error = NSError()
                
                if response.response?.statusCode == 500 {
                    error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 401 {
                    error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 404 {
                    error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                } else {
                    error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                }
                
                failure(error)
            }
        }
    }
    
    
    
    
//    
//    func getPeople(type: TypeUserSearch, uuid: String, success:@escaping ([LoginModel]) -> Void,failure: @escaping FailureWithError) {
//        let prepareParameters = ["type":type.rawValue, "uuid": uuid] as [String : Any]
//        
//    }

    
    
    
    
    func getUserFeed(sortType: SortTypeEvent, success:@escaping SuccessWithEvents, failure: @escaping FailureWithError) {
        let prepareParameters = ["sort_type":sortType.rawValue] as [String : Int]
        let finalParameters = convertParameterToServerFormat(parameters: prepareParameters)
        
        Alamofire.request(APIUrls.userFeed, method: .post, parameters: finalParameters, encoding: URLEncoding.httpBody, headers: TokenManager.sharedInstance.token()).responseJSON { (response) in
            switch response.result {
            case .success(let value):let json = JSON(value)
            var events = [Event]()
            
            if json.array?.count != nil {
                for event in json.array! {
                    events.append(EventManager.sharedManager.parseEventResponse(json: event))
                }
            }
        
            success(events)
                
            case .failure(_):
                var error = NSError()
                
                if response.response?.statusCode == 500 {
                    error = NSError(domain: "Внутреняя ошибка", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 401 {
                    error = NSError(domain: "Токен не верен, авторизуйтесь заново", code: (response.response?.statusCode)!, userInfo: nil)
                } else if response.response?.statusCode == 404 {
                    error = NSError(domain: "Ошибка пользователя", code: (response.response?.statusCode)!, userInfo: nil)
                } else {
                    error = NSError(domain: "Нет соединения с сервером", code: (response.response?.statusCode)!, userInfo: nil)
                }
                
                failure(error)
            }
        }
    }
    
    
    
    
    private func parseUserResponse(json: JSON) -> UserModel {
        let model = UserModel()
        
        if userTypeData == .base {
            model.name = json["name"].string!
            model.description = json["description"].string!
            model.sex = json["sex"].int!
            model.avatarURL = json["avatar_url"].string!
            
        } else if userTypeData == .contact {
            model.email = json["email"].string!
        } else if userTypeData == .geo {
            model.country = json["country"].string!
        } else if userTypeData == .likes {
            model.category = json["category"].arrayObject as! [Int]
        }
       
        
        return model
    }
}
