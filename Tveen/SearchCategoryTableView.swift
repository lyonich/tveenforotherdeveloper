//
//  SearchCategoriesTableView.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 21/08/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit


class SearchCategoryTableView: UITableViewController {
    
    
  var selectedCategories = [Category]()
    
    let categoryNames = ["Развлечения и отдых", "Ночная жизнь", "Спорт", "Искусство", "Образование и бизнес", "Для детей", "Другое"]
    let categoryImages = ["category-20.png","category-13.png","category-15.png","category-16.png","category-14.png","category-18.png","category-21.png"]
    
    override func viewDidLoad(){
        super.viewDidLoad()
    
        setupNavigationController()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryNames.count
    }
    
    func setupNavigationController(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Далее", style: .done, target: self, action:#selector(nextScreen))
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:      "burger@2x.png"), landscapeImagePhone: UIImage(named: "burger@2x.png"), style: .plain, target: self, action: #selector(openMenu))
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 480, height: 44))
        label.numberOfLines = 2
        label.text =  "Выберите категории"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont(name: "SFUI-Regular", size: 18)
        
        navigationItem.titleView = label
    }
    
    func nextScreen(){
        performSegue(withIdentifier: "next", sender: self)
    }
    
    func cancel(){
        dismiss(animated: true, completion: nil)
    }
    
    func openMenu(){
        self.evo_drawerController?.toggleDrawerSide(.left, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let button = UIBarButtonItem()
        button.title = ""
        button.style = .plain
        navigationItem.backBarButtonItem = button
        
        (segue.destination as! SearchViewController).selectedCategories = selectedCategories
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back@2x.png")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCategoryCell", for: indexPath) as! SearchCategoriesCell
        
        cell.name.text = categoryNames[indexPath.row]
        cell.im.image = UIImage(named: categoryImages[indexPath.row])?.withRenderingMode(.alwaysTemplate)
        cell.im.tintColor = UIColor.gray
        
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        (tableView.cellForRow(at: indexPath) as! SearchCategoriesCell).name.textColor = UIColor.black
        
        selectedCategories.append(Category.categories[indexPath.row])
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
         (tableView.cellForRow(at: indexPath) as! SearchCategoriesCell).name.textColor = UIColor.lightGray
        
        for i in 0..<selectedCategories.count{
            if selectedCategories[i].rawValue == indexPath.row{
                selectedCategories.remove(at: i)
            }
        }
    }

}
