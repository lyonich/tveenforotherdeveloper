//
//  APIUrls.swift
//  Tveen
//
//  Created by Lyonich on 14.03.17.
//  Copyright © 2017 Kirill Dobryakov. All rights reserved.
//

import Foundation

struct APIUrls {
    static let base = "http://35.157.243.187:9997"
    
    static let registration     = base + "/user/sign_up/"
    static let login            = base + "/user/sign_in/"
    static let event            = base + "/event/"
    static let user             = base + "/user/edit/"
    static let follow           = base + "/user/follow/"
    static let unfollow         = base + "/user/unfollow/"
    static let blacklist        = base + "/user/blacklist/"
    static let availability     = base + "/user/availability/"
    static let localization     = base + "/user/localization/"
    static let help             = base + "/user/help/"
    static let password         = base + "/user/password/"
    static let clear            = base + "/user/clear/"
    static let delete           = base + "/user/delete/"
    static let userSearch       = base + "/user/search/"
    static let userFeed         = base + "/user/feed/"
}
