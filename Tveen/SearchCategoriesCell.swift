
//
//  SearchCategoriesCell.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 21/08/16.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit

class SearchCategoriesCell: UITableViewCell{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var im: UIImageView!
    
    @IBOutlet weak var selectedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        selectedImageView?.image = selected ? #imageLiteral(resourceName: "checkmark") : nil
    }
}
