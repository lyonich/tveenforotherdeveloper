//
//  FeedLCell.swift
//  Tveen
//
//  Created by Kirill Dobryakov on 24/11/2016.
//  Copyright © 2016 Kirill Dobryakov. All rights reserved.
//

import UIKit
import NWSTokenView


protocol ShowProfileDelegate {
    func showProfile(id: String)
}

class FeedLCell: UITableViewCell {
    
    @IBOutlet weak var creatorLogin: UILabel!
    
    @IBOutlet weak var labelConstraint: NSLayoutConstraint!
    @IBOutlet weak var creatorIm: UIImageView!
    @IBOutlet weak var creatorName: UILabel!
    @IBOutlet weak var tokenViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var willGo: UILabel!
    @IBOutlet weak var about: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var categoryIm: UIImageView!
    @IBOutlet weak var im: UIImageView!
    
    @IBOutlet weak var tags: NWSTokenView!
    
    @IBOutlet weak var willGoIm: UIImageView!
    @IBOutlet weak var dateLabelIm: UIImageView!
   
    var myTags = [String]()
    
    var showProgileDelegate: ShowProfileDelegate?
    
    let tokenViewMinHeight: CGFloat = 31
    let tokenViewMaxHeight: CGFloat = 150.0

    var owner_UUID = ""
   
    var aboutString: String?{
        didSet{
            if about != nil{
                about.text = aboutString
                labelConstraint.constant = about.requiredHeight()
                layoutIfNeeded()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryIm.layer.shadowColor = UIColor.black.cgColor
        categoryIm.layer.shadowOffset = CGSize(width: 0, height: 0)
        categoryIm.layer.shadowOpacity = 0.5
        categoryIm.layer.shadowRadius = 5
        categoryIm.clipsToBounds = false
    }
    
    //MARK: Actions
    @IBAction func showProfilePressed(_ sender: Any) {
        showProgileDelegate?.showProfile(id: owner_UUID)
    }
}

extension FeedLCell: NWSTokenDataSource, NWSTokenDelegate{
    // MARK: NWSTokenDataSource
    func numberOfTokensForTokenView(_ tokenView: NWSTokenView) -> Int{
        return myTags.count
    }
    
    func insetsForTokenView(_ tokenView: NWSTokenView) -> UIEdgeInsets?{
        return UIEdgeInsetsMake(10, 0, 10, 5.5)
    }
    
    func titleForTokenViewLabel(_ tokenView: NWSTokenView) -> String?{
        return ""
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: NWSTokenView) -> String?{
        return ""
    }
    
    func tokenView(_ tokenView: NWSTokenView, viewForTokenAtIndex index: Int) -> UIView?{
        if let token = OpenEventToken.initWithTitle(myTags[Int(index)], image: nil){
            return token
        }
        
        return nil
    }
    
    // MARK: NWSTokenDelegate
    func tokenView(_ tokenView: NWSTokenView, didSelectTokenAtIndex index: Int){
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeselectTokenAtIndex index: Int){
    }
    
    func tokenView(_ tokenView: NWSTokenView, didDeleteTokenAtIndex index: Int){
        if index < self.myTags.count{
            self.myTags.remove(at: Int(index))
            tokenView.reloadData()
            tokenView.layoutIfNeeded()
        }
    }
    
    func tokenView(_ tokenViewDidBeginEditing: NWSTokenView){
    }
    
    func tokenViewDidEndEditing(_ tokenView: NWSTokenView){
        self.tokenView(tokenView, didChangeText: tokenView.textView.text +  " ")
    }
    
    func tokenView(_ tokenView: NWSTokenView, didChangeText text: String){
        if text.replacingOccurrences(of: " ", with: "").characters.count > 0{
            let lastChar = text[text.characters.index(before: text.endIndex)]
            if lastChar == " "{
                let newTag = tokenView.textView.text.trimmingCharacters(in: CharacterSet.whitespaces)
                self.myTags.append(newTag)
                
                tokenView.textView.text = ""
                tokenView.reloadData()
            }
        }
    }
    
    func tokenView(_ tokenView: NWSTokenView, didEnterText text: String){
    }
    
    func tokenView(_ tokenView: NWSTokenView, contentSizeChanged size: CGSize){
        self.tokenViewHeightConstraint.constant = max(tokenViewMinHeight,min(size.height, self.tokenViewMaxHeight))
    }
    
    func tokenView(_ tokenView: NWSTokenView, didFinishLoadingTokens tokenCount: Int){
    }
}

extension UILabel{
    
    func requiredHeight() -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: self.frame.width, height: CGFloat.greatestFiniteMagnitude)))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }
}

